<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefeciary extends Model
{
    //
    public function member()
    {
        return $this->belongsTO('App\Member', 'member_id', 'id');
    }
}
