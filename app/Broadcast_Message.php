<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broadcast_Message extends Model
{
    //
    public function member()
    {
        return $this->belongsTO('App\Member', 'member_id', 'id');
    }

    public function broadcast()
    {
        return $this->belongsTO('App\Broadcast', 'broadcast_id', 'id');
    }
}
