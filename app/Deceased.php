<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deceased extends Model
{
    //

    public function member()
    {
        return $this->belongsTO('App\Member', 'member_id', 'id');
    }
    public function deceased_member()
    {
        return $this->belongsTO('App\Member', 'deceased_id', 'id');
    }
    public function deceased_beneficiary()
    {
        return $this->belongsTO('App\Benefeciary', 'deceased_id', 'id');
    }
    
}
