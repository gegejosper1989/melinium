<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Benefeciary;
use App\User;
use App\Payment;
use App\Setting;
use Illuminate\Support\Facades\Hash;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use DB;
class AccountController extends Controller
{
    //
    public function add_member(Request $req){
        
        $rules = array(
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'mname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:255|unique:users',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator->getMessageBag());
        }
        else {
            $data_user = new User();
            $data_user->name = strtoupper($req['lname'].', '.$req['fname'].' '.$req['mname']) ;
            $data_user->username = $req['username'];
            $data_user->email = $req['email'];
            $data_user->password = Hash::make($req['password']);
            $data_user->usertype = 'member';
            $data_user->status = 'active';
            $data_user->save();

            $data_member = new Member();
            $data_member->user_id = $data_user->id;
            $data_member->fname = $req['fname'];
            $data_member->lname = $req['lname'];
            $data_member->mname = $req['mname'];
            $data_member->dob = $req['dob'];
            $data_member->address = $req['address'];
            $data_member->gender = $req['gender'];
            $data_member->civil_status = $req['civil_status'];
            $data_member->profilepic = 'profile.jpg';
            $data_member->contact_number = $req['contact_number'];
            $data_member->member_type = $req['member_type'];
            $data_member->status = 'active';
            $data_member->save();

            $data_setting = Setting::first();

            $data_payment = new Payment();
            $data_payment->member_id = $data_member->id;
            $data_payment->deceased_id = 0;
            $data_payment->amount = $data_setting->registration_fee;
            $data_payment->user_id = $data_user->id;
            $data_payment->member_type = 'member';
            $data_payment->payment_type = 'membership';
            $data_payment->payment_date = date('Y-m-d');
            $data_payment->payment_status = 'paid';
            $data_payment->save();
        }
        return redirect()->back()->with('success','Member successfully added!');
    }
    public function edit_member(Request $req){
        $data_member = Member::find($req->id);
        $data_member->fname = $req->fname;
        $data_member->mname = $req->mname;
        $data_member->lname = $req->lname;
        $data_member->dob = $req->dob;
        $data_member->address = $req->address;
        $data_member->gender = $req->gender;
        $data_member->contact_number = $req->contact_number;
        $data_member->civil_status = $req->civil_status;
        $data_member->member_type = $req->member_type;
        $data_member->save();
        $req_member = Member::where('id', '=', $req->id)->first();

        $update_user = User::where('id', '=', $req_member->user_id)
                    ->update(['name' => $req->lname.", ".$req->fname." ".$req->mname]);
        return response()->json($req);
    }
    public function view_member($member_id){
        $data_member = Member::with('user')->where('id', '=', $member_id)->first();
        $data_beneficiary = Benefeciary::where('member_id', '=', $member_id)->get();
        return view('admin.member', compact('data_member', 'data_beneficiary'));

    }
    public function activate($member_id){
        $member = Member::with('user')->where('id', '=', $member_id)->first();
        $data_member = Member::find(1);

        $data_member = Member::where('id', '=', $member_id)
                    ->update(['status' => 'active']);
    
        return redirect()->back();
    }
    public function deactivate($member_id){
        $member = Member::with('user')->where('id', '=', $member_id)->first();
        $data_member = Member::where('id', '=', $member_id)
                    ->update(['status' => 'inactive']);
    
        return redirect()->back();
    }

    public function delete_member(Request $req){
        $member = Member::where('id', '=', $req->id)->first();
        //$user = User::where('id','=',$member->user_id)->delete();
        //dd($member);
        DB::table('users')->where('id','=', $member->user_id)->delete();
        Member::find($req->id)->delete();
        
        //return redirect()->back()->with('warning','Member successfully deleted!');
        return response()->json($req);
    }
}
