<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Setting;
use App\Payment;
use App\Benefeciary;
use App\Event;
use App\Broadcast;
use App\Deceased;
use App\User;
use DB; 
use Carbon\Carbon;
class AdminController extends Controller
{
    //
    public function index(){
        $data_setting = Setting::first();
        $data_member = Member::where('status', '=', 'active')->count();
        $data_inactive_member = Member::where('status', '=', 'inactive')->count();
        $data_applicant_member = Member::where('member_type', '=', 'applicant')->count();
        $data_regular_member = Member::where('member_type', '=', 'regular')->count();
        $data_nonregular_member = Member::where('member_type', '=', 'non-regular')->count();
        $data_payment = Payment::where('payment_status', '=', 'paid')->whereDate('payment_date', Carbon::today())->get();
        $data_count_nonregular_member = Member::where('member_type','=','non-regular')->get();
        $to_be_regular_count = 0;
        foreach($data_count_nonregular_member as  $to_be_regular){
            $created = new Carbon ($to_be_regular->created_at);
            $now = Carbon::now();
            $difference = ($created->diff($now)->days < 1)
                ? 0
                : $created->diffInDays($now);
            $agecount = Carbon::parse($to_be_regular->dob)->diff(Carbon::now())->format('%y');
            if($agecount < $data_setting->from_age){
                $probation_days = $data_setting->standard_duration;  
            }
            else {
                $probation_days = $data_setting->duration;  
            } 
            if($to_be_regular->member_type == 'non-regular')
                if($difference >=$probation_days)  {
                    $to_be_regular_count = $to_be_regular_count + 1;   
                }                                   
        }
        $recent_payment = 0;
        foreach($data_payment as $Payment){
            $recent_payment = $recent_payment + $Payment->amount;
        }
        //dd($recent_payment);
        return view('admin.dashboard', compact('data_member', 'data_inactive_member', 'data_applicant_member', 'recent_payment', 'data_regular_member', 'data_nonregular_member', 'to_be_regular_count'));
    }

    public function members(){
        $data_member = Member::latest()->paginate(50);
        return view('admin.members', compact('data_member'));
    }
    public function deceased(){
        $data_member = Member::where('status', '=', 'dead')->latest()->paginate(50);
        $data_beneficiary = Benefeciary::where('status', '=', 'dead')->with('member')->latest()->paginate(50);
        return view('admin.deceased', compact('data_member', 'data_beneficiary'));
    }
    public function payments(){
        $data_payment = Payment::with('member', 'deceased_member', 'deceased_beneficiary')->latest()->paginate(50);
        //dd($data_payment);
        return view('admin.payments', compact('data_payment'));
    }
    public function claims(){
        $data_deceased = Deceased::with('member', 'deceased_member', 'deceased_beneficiary')->latest()->paginate(50);
        //dd($data_payment);
        return view('admin.claimed', compact('data_deceased'));
    }
    public function notifications(){
        
        return view('admin.notifications');
    }
    public function reports(){
        $data_payment = Payment::where('payment_status', '=', 'paid')->whereDate('payment_date', Carbon::today())->get();
        $data_payment_month = Payment::where('payment_status', '=', 'paid')->whereMonth('created_at', '=', date('m'))->get();
        $total_monthly_payment = 0;
        foreach($data_payment_month as $monthly_payment){
            $total_monthly_payment = $total_monthly_payment + $monthly_payment->amount;
        }
        $data_deceased_member = Member::where('status', '=', 'dead')->count();
        $data_deceased_beneficiary = Benefeciary::where('status', '=', 'dead')->count();
        $data_total_member = Member::where('member_type', '!=', 'applicant')->count();
        return view('admin.reports', compact('data_payment', 'total_monthly_payment', 'data_deceased_member', 'data_total_member', 'data_deceased_beneficiary'));
    }

    public function settings(){
        $data_setting = Setting::first();
        return view('admin.settings', compact('data_setting'));
    }
    public function settings_update(Request $req){
        $data_setting = Setting::find($req->setting_id);
        $data_setting->from_age = $req->from_age;
        $data_setting->to_age = '0';
        $data_setting->duration = $req->duration;
        $data_setting->registration_fee = $req->registration_fee;
        $data_setting->limit_days = $req->limit_days;
        $data_setting->standard_fee = $req->standard_fee;
        $data_setting->standard_duration = $req->standard_duration;
        $data_setting->save();
        return redirect()->back()->with('success','Settings successfully updated!');
    }

    public function broadcasts(){
        $data_broadcast = Broadcast::latest()->paginate(20);
        return view('admin.broadcasts', compact('data_broadcast'));
    }
    public function staffs(){
        $data_user = User::where('usertype','!=', 'member')->paginate(20);
        return view('admin.staffs', compact('data_user'));
    }
    public function events(){
        $data_event = Event::latest()->paginate(20);
        return view('admin.events', compact('data_event'));
    }
    public function registrations(){
        $data_setting = Setting::first();
        $data_member = Member::where('member_type','=','non-regular')->orWhere('member_type','=','applicant')->latest()->paginate(50);
        return view('admin.registrations', compact('data_member', 'data_setting'));
    }

    public function members_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_member = DB::table('members')
                ->where(function($query) use ($search){
                    $query->where('members.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_member)
            {
                if(count($data_member) >0 ){
                    foreach ($data_member as $Member) {
                        
                        $output .='<tr class="row'.$Member->id.'">
                        <td><a href="/admin/member/'.$Member->id.'">'.strtoupper($Member->lname).', '.strtoupper($Member->fname).' '.strtoupper($Member->mname).'</a></td>
                        <td>'.$Member->address.'</td>
                        <td>'.$Member->member_type.'</td>
                        <td>'.$Member->status.'</td>
                        <td>
                            <a href="/admin/member/'.$Member->id.'" class="btn btn-info btn-small" ><i class="btn-icon-only fas fa-search"> </i></a>
                            <a href="javascript:;" class="edit-member btn btn-small btn-success" 
                            data-id="'.$Member->id.'" 
                            data-fname="'.$Member->fname.'"
                            data-lname="'.$Member->lname.'"
                            data-mname="'.$Member->mname.'"
                            data-dob="'.$Member->dob.'"
                            data-contact_number="'.$Member->contact_number.'"
                            data-address="'.$Member->address.'"
                            data-gender="'.$Member->gender.'">
                            <i class="btn-icon-only fas fa-pen"> </i></a>
                        </td>
                        </tr>';
                    }
                }
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
        }
    }
    public function payments_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_member = DB::table('members')
                ->where(function($query) use ($search){
                    $query->where('members.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_member)
            {
                if(count($data_member) > 0 ){
                    foreach ($data_member as $Member) {
                        $data_payment = Payment::with('member', 'deceased_member', 'deceased_beneficiary')->where('member_id', '=', $Member->id)->where('payment_status', '=', 'not paid')->get();
                        //dd($data_payment);
                        foreach($data_payment as $Payment){
                            $output .='<tr class="row'.$Payment->member->id.'">
                            <td>'.$Payment->created_at->format('d-m-Y').'</td>
                            <td><a href="/admin/member/'.$Payment->member->id.'">'.strtoupper($Payment->member->lname).', '.strtoupper($Payment->member->fname).' '.strtoupper($Payment->member->mname).'</a></td>
                            <td>'.$Payment->amount.'</td>
                            <td>'.$Payment->payment_type.'</td>';
                            if($Payment->member_type == 'member' && $Payment->payment_type == 'deceased'){
                                $output .= '<td><a href="/admin/deceased/{{$Payment->deceased_id}}">'.strtoupper($Payment->deceased_member->lname).', '.strtoupper($Payment->deceased_member->fname).' '.strtoupper($Payment->deceased_member->mname).'</a></td>';
                            }
                             
                            elseif($Payment->member_type == 'beneficiary' && $Payment->payment_type == 'deceased'){
                                
                                $output .= '<td><a href="/admin/deceased/'.$Payment->deceased_id.'"> '.strtoupper($Payment->deceased_beneficiary->lname).', '.strtoupper($Payment->deceased_beneficiary->fname).' '.strtoupper($Payment->deceased_beneficiary->mname).'</a>';
                            }
                               
                            else{

                            }

                           $output .= '<td>'.$Payment->payment_status.'</td>';
                           
                            if($Payment->member_type == 'member' && $Payment->payment_type == 'deceased'){
                                if($Payment->payment_status != 'paid'){
                                    $output .='<td>
                                        <a href="javascript:;" class="pay-member btn btn-small btn-success" 
                                        data-id="'.$Payment->id.'" 
                                        data-fname="'.$Payment->member->fname.'"
                                        data-lname="'.$Payment->member->lname.'"
                                        data-mname="'.$Payment->member->mname.'"
                                        data-dfname="'.$Payment->deceased_member->fname.'"
                                        data-dlname="'.$Payment->deceased_member->lname.'"
                                        data-dmname="'.$Payment->deceased_member->mnam.'"
                                        data-amount="'.$Payment->amount.'"
                                        >
                                        <i class="btn-icon-only far fa-money-bill-alt"></i>
                                        </a>
                                    </td>';
                                }
                            }
                            elseif($Payment->member_type == 'beneficiary' && $Payment->payment_type == 'deceased'){
                                if($Payment->payment_status != 'paid' && $Payment->payment_status != 'forfeit'){
                                    $output .= '<td>
                                    <a href="javascript:;" class="pay-member btn btn-small btn-success" 
                                    data-id="'.$Payment->id.'" 
                                    data-fname="'.$Payment->member->fname.'"
                                    data-lname="'.$Payment->member->lname.'"
                                    data-mname="'.$Payment->member->mname.'"
                                    data-dfname="'.$Payment->deceased_beneficiary->fname.'"
                                    data-dlname="'.$Payment->deceased_beneficiary->lname.'"
                                    data-dmname="'.$Payment->deceased_beneficiary->mname.'"
                                    data-amount="'.$Payment->amount.'"
                                    >
                                    <i class="btn-icon-only far fa-money-bill-alt"></i>
                                    </a>
                                    </td>';
                                }
                            }
                            else {

                            }
                            $output .= '</tr>';
                        }
                        
                    }
                }
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
        }
    }
}
