<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Member;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/thank-you';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //dd($data);
        $data_user = new User();
        $data_user->name = strtoupper($data['lname'].' '.$data['fname']) ;
        $data_user->username = $data['username'];
        $data_user->email = $data['email'];
        $data_user->password = Hash::make($data['password']);
        $data_user->usertype = 'member';
        $data_user->status = 'active';
        $data_user->save();

        $data_member = new Member();
        $data_member->user_id = $data_user->id;
        $data_member->fname = $data['fname'];
        $data_member->lname = $data['lname'];
        $data_member->mname = ' ';
        $data_member->dob = $data['dob'];
        $data_member->gender = $data['gender'];
        $data_member->address = $data['address'];
        $data_member->civil_status = $data['civil_status'];
        $data_member->member_type = 'applicant';
        $data_member->contact_number = $data['contact_number'];
        $data_member->profilepic = 'profile.jpg';
        $data_member->status = 'active';
        $data_member->save();
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => Hash::make($data['password']),
        // ]);
        return $data_user;
    }
}
