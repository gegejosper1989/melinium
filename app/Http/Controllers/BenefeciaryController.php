<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Benefeciary;

class BenefeciaryController extends Controller
{
    //
    public function add_benefeciaries(Request $req){
        $data_benefeciary = new Benefeciary();
        $data_benefeciary->member_id = $req->member_id;
        $data_benefeciary->fname = $req->fname;
        $data_benefeciary->lname = $req->lname;
        $data_benefeciary->mname = $req->mname;
        $data_benefeciary->relationship = $req->relationship;
        $data_benefeciary->dob = $req->bdate;
        $data_benefeciary->gender = $req->gender;
        $data_benefeciary->status = 'active';
        $data_benefeciary->save();

        return redirect()->back()->with('success','Beneficiary successfully added!');
    }

    public function remove_beneficiary($beneficiary_id){
        
    }

    public function edit_benefeciaries(Request $req){
        $data_beneficiary = Benefeciary::find($req->id);
        $data_beneficiary->fname = $req->fname;
        $data_beneficiary->mname = $req->mname;
        $data_beneficiary->lname = $req->lname;
        $data_beneficiary->dob = $req->dob;      
        $data_beneficiary->gender = $req->gender;
        $data_beneficiary->relationship = $req->relationship;
        $data_beneficiary->save();
        return response()->json($req);
    }
}
