<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broadcast;
use App\Broadcast_Message;
use App\Member;
use Twilio\Rest\Client;
class BroadcastController extends Controller
{
    //
    public function add_broadcast(Request $req){
        //dd(config('app.twilio_number'));
        
        // if($req->message_type[0]){
            
        // }
        $data_broadcast = new Broadcast();
        $data_broadcast->broadcast_type = $req->receiver;
        $data_broadcast->subject = $req->subject;
        $data_broadcast->message = $req->message;
        $data_broadcast->status = 'send';
        $data_broadcast->save();

        if($req->receiver == 'regular'){
            $data_member = Member::where('member_type', '=', 'regular')->where('member_type', '!=', 'applicant')->get();
        }
        elseif($req->receiver == 'non-regular'){
            $data_member = Member::where('member_type', '=', 'non-regular')->where('member_type', '!=', 'applicant')->get();
        }
        elseif($req->receiver == 'applicant'){
            $data_member = Member::where('member_type', '=', 'applicant')->get();
        }
        elseif($req->receiver == 'active'){
            $data_member = Member::where('status', '=', 'active')->where('member_type', '!=', 'applicant')->get();
        }
        elseif($req->receiver == 'inactive'){
            $data_member = Member::where('status', '=', 'inactive')->where('member_type', '!=', 'applicant')->get();
        }
        else{
            $data_member = Member::get();
        }
        $account_sid = config('app.twilio_sid');
        $auth_token = config('app.twilio_auth_token');
        $twilio_number = config('app.twilio_number');
        $message = $req->message; 
        // $recipients = '+639177222631';
    
        // $client = new Client($account_sid, $auth_token);
        // $client->messages->create($recipients, 
        //         ['from' => $twilio_number, 'body' => $message] );
       
        if(count($req->message_type != 0)){
            foreach($req->message_type as $Type){
                if($Type == 'account'){
                    foreach($data_member as $Member){
                        $data_broadcast_message = new Broadcast_Message();
                        $data_broadcast_message->broadcast_id = $data_broadcast->id;
                        $data_broadcast_message->member_id = $Member->id;
                        $data_broadcast_message->broadcast_reciever = $req->receiver;
                        $data_broadcast_message->status = 'unread';
                        $data_broadcast_message->save();
                    }
                }
                elseif($Type == 'sms'){
                    foreach($data_member as $Member){
                        $recipients = $Member->contact_number;
                        $client = new Client($account_sid, $auth_token);
                        $client->messages->create($recipients, 
                                ['from' => $twilio_number, 'body' => $message] );
                    }
                } 
                else {
    
                }
            }
        }
        
        return redirect()->back()->with('success','Broadcast successfully distritbuted!');
    }
}
