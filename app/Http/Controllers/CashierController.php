<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Setting;
use App\Payment;
use App\Benefeciary;
use App\Event;
use App\Broadcast;
use App\Deceased;
use App\User;
use DB; 
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CashierController extends Controller
{
    //
    public function index(){
        $data_member = Member::where('status', '=', 'dead')->latest()->paginate(50);
        $data_beneficiary = Benefeciary::where('status', '=', 'dead')->with('member')->latest()->paginate(50);
        return view('cashier.home', compact('data_member', 'data_beneficiary'));
    }
    public function members(){
        $data_member = Member::latest()->paginate(50);
        return view('cashier.members', compact('data_member'));
    }
    public function view_member($member_id){
        $data_member = Member::with('user')->where('id', '=', $member_id)->first();
        $data_beneficiary = Benefeciary::where('member_id', '=', $member_id)->get();
        return view('cashier.member', compact('data_member', 'data_beneficiary'));

    }
    public function payments(){
        $data_payment = Payment::with('member', 'deceased_member', 'deceased_beneficiary')->latest()->paginate(50);
        return view('cashier.payments', compact('data_payment'));
    }
    public function notifications(){
        
        return view('cashier.notifications');
    }
    public function reports(){
        
        $data_payment = Payment::where('payment_status', '=', 'paid')->whereDate('payment_date', Carbon::today())->get();
        $data_payment_month = Payment::where('payment_status', '=', 'paid')->whereMonth('created_at', '=', date('m'))->get();
        $total_monthly_payment = 0;
        foreach($data_payment_month as $monthly_payment){
            $total_monthly_payment = $total_monthly_payment + $monthly_payment->amount;
        }
        $data_deceased_member = Member::where('status', '=', 'dead')->count();
        $data_total_member = Member::where('member_type', '!=', 'applicant')->count();
        return view('cashier.reports', compact('data_payment', 'total_monthly_payment', 'data_deceased_member', 'data_total_member'));
    }

    public function setting(){
        if (Auth::check()){
            $userId = Auth::user()->id;
        }
        $data_user = User::where('id', '=', $userId)->first();
        return view('cashier.setting', compact('data_user'));
    }

    public function members_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_member = DB::table('members')
                ->where(function($query) use ($search){
                    $query->where('members.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_member)
            {
                if(count($data_member) >0 ){
                    foreach ($data_member as $Member) {
                        
                        $output .='<tr class="row'.$Member->id.'">
                        <td><a href="/cashier/member/'.$Member->id.'">'.strtoupper($Member->lname).', '.strtoupper($Member->fname).' '.strtoupper($Member->mname).'</a></td>
                        <td>'.$Member->address.'</td>
                        <td>'.$Member->member_type.'</td>
                        <td>'.$Member->status.'</td>
                        <td>
                            <a href="/cashier/member/'.$Member->id.'" class="btn btn-info btn-small" ><i class="btn-icon-only fas fa-search"> </i></a>
                            
                        </td>
                        </tr>';
                    }
                }
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
        }
    }

    public function payments_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_member = DB::table('members')
                ->where(function($query) use ($search){
                    $query->where('members.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_member)
            {
                if(count($data_member) > 0 ){
                    foreach ($data_member as $Member) {
                        $data_payment = Payment::with('member', 'deceased_member', 'deceased_beneficiary')->where('member_id', '=', $Member->id)->where('payment_status', '=', 'not paid')->get();
                        //dd($data_payment);
                        foreach($data_payment as $Payment){
                            $output .='<tr class="row'.$Payment->member->id.'">
                            <td>'.$Payment->created_at->format('d-m-Y').'</td>
                            <td><a href="/admin/member/'.$Payment->member->id.'">'.strtoupper($Payment->member->lname).', '.strtoupper($Payment->member->fname).' '.strtoupper($Payment->member->mname).'</a></td>
                            <td>'.$Payment->amount.'</td>
                            <td>'.$Payment->payment_type.'</td>';
                            if($Payment->member_type == 'member' && $Payment->payment_type == 'deceased'){
                                $output .= '<td><a href="/admin/deceased/{{$Payment->deceased_id}}">'.strtoupper($Payment->deceased_member->lname).', '.strtoupper($Payment->deceased_member->fname).' '.strtoupper($Payment->deceased_member->mname).'</a></td>';
                            }
                             
                            elseif($Payment->member_type == 'beneficiary' && $Payment->payment_type == 'deceased'){
                                
                                $output .= '<td><a href="/admin/deceased/'.$Payment->deceased_id.'"> '.strtoupper($Payment->deceased_beneficiary->lname).', '.strtoupper($Payment->deceased_beneficiary->fname).' '.strtoupper($Payment->deceased_beneficiary->mname).'</a>';
                            }
                               
                            else{

                            }

                           $output .= '<td>'.$Payment->payment_status.'</td>';
                           
                            if($Payment->member_type == 'member' && $Payment->payment_type == 'deceased'){
                                if($Payment->payment_status != 'paid'){
                                    $output .='<td>
                                        <a href="javascript:;" class="pay-member btn btn-small btn-success" 
                                        data-id="'.$Payment->id.'" 
                                        data-fname="'.$Payment->member->fname.'"
                                        data-lname="'.$Payment->member->lname.'"
                                        data-mname="'.$Payment->member->mname.'"
                                        data-dfname="'.$Payment->deceased_member->fname.'"
                                        data-dlname="'.$Payment->deceased_member->lname.'"
                                        data-dmname="'.$Payment->deceased_member->mnam.'"
                                        data-amount="'.$Payment->amount.'"
                                        >
                                        <i class="btn-icon-only far fa-money-bill-alt"></i>
                                        </a>
                                    </td>';
                                }
                            }
                            elseif($Payment->member_type == 'beneficiary' && $Payment->payment_type == 'deceased'){
                                if($Payment->payment_status != 'paid' && $Payment->payment_status != 'forfeit'){
                                    $output .= '<td>
                                    <a href="javascript:;" class="pay-member btn btn-small btn-success" 
                                    data-id="'.$Payment->id.'" 
                                    data-fname="'.$Payment->member->fname.'"
                                    data-lname="'.$Payment->member->lname.'"
                                    data-mname="'.$Payment->member->mname.'"
                                    data-dfname="'.$Payment->deceased_beneficiary->fname.'"
                                    data-dlname="'.$Payment->deceased_beneficiary->lname.'"
                                    data-dmname="'.$Payment->deceased_beneficiary->mname.'"
                                    data-amount="'.$Payment->amount.'"
                                    >
                                    <i class="btn-icon-only far fa-money-bill-alt"></i>
                                    </a>
                                    </td>';
                                }
                            }
                            else {

                            }
                            $output .= '</tr>';
                        }
                        
                    }
                }
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
        }
    }

    public function save_setting(Request $req){
        $update_user = User::where('id', '=', $req->user_id)
                    ->update(['name' => $req->name, 'username' => $req->username, 'email' => $req->email]);

        if(!empty($req->password)){
                $update_pass = User::where('id', '=', $req->user_id)
            ->update(['password' => Hash::make($req->password)]);
        }

        return redirect()->back()->with('success','Account successfully updated!');
        
    }

}
