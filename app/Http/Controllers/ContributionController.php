<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contribution;
use App\Payment;
use App\Deceased;

class ContributionController extends Controller
{
    //
    public function process_contribution(Request $req){
        $data_contribution = new Contribution();
        $data_contribution->deceased_id = $req->deceased_id;
        $data_contribution->deceased_member_id = $req->deceased_member_id;
        $data_contribution->paid = $req->paid;
        $data_contribution->unpaid = $req->unpaid;
        $data_contribution->member_paid = $req->member_paid;
        $data_contribution->member_unpaid = $req->member_unpaid;
        $data_contribution->service_fee = $req->service_fee;
        $data_contribution->member_type = $req->member_type;
        $data_contribution->member_recievable = $req->member_recievable;
        $data_contribution->save();

        $update_deceased = Deceased::where('id', '=', $req->deceased_id)
        ->update(['status' => 'paid', 'amount_received' => $req->member_recievable]);

        $update_payment = Payment::where('deceased_id', '=', $req->deceased_member_id)->where('payment_status', '=', 'not paid')
                    ->update(['payment_status' => 'forfeit', 'payment_date' => date('Y-m-d')]);
        return response()->json();

    }

    public function process_contribution_cashier(Request $req){
        $data_contribution = new Contribution();
        $data_contribution->deceased_id = $req->deceased_id;
        $data_contribution->deceased_member_id = $req->deceased_member_id;
        $data_contribution->paid = $req->paid;
        $data_contribution->unpaid = $req->unpaid;
        $data_contribution->member_paid = $req->member_paid;
        $data_contribution->member_unpaid = $req->member_unpaid;
        $data_contribution->service_fee = $req->service_fee;
        $data_contribution->member_type = $req->member_type;
        $data_contribution->member_recievable = $req->member_recievable;
        $data_contribution->save();
        $update_deceased = Deceased::where('id', '=', $req->deceased_id)
        ->update(['status' => 'paid', 'amount_received' => $req->member_recievable]);

        $update_payment = Payment::where('deceased_id', '=', $req->deceased_member_id)->where('payment_status', '=', 'not paid')
                    ->update(['payment_status' => 'forfeit', 'payment_date' => date('Y-m-d')]);
        return response()->json();
    }

    
}
