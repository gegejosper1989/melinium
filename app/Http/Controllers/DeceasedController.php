<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 
use App\Member;
use App\Payment;
use App\Setting;
use App\Deceased;
use App\Benefeciary;


class DeceasedController extends Controller
{
    //
    public function member_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_member = DB::table('members')
                ->where(function($query) use ($search){
                    $query->where('members.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_member)
            {
                if(count($data_member) >0 ){
                    foreach ($data_member as $Member) {
                        $output.='<tr>
                            <td><a href="/admin/member/'.$Member->id.'">'.strtoupper($Member->lname).', '.strtoupper($Member->fname).' '.strtoupper($Member->mname).'</td>';
                        if($Member->status == 'active' && $Member->member_type == 'regular' ){
                            $output .='<td><a href="javascript:;" class=" btn btn-info btn-small add_deceased" data-fname="'.$Member->fname.'" data-lname="'.$Member->lname.'" data-mname="'.$Member->mname.'" data-id="'.$Member->id.'" data-member_type="member"><i class="btn-icon-only fas fa-plus"> </i></a></td>';
                        }
                        else{
                            $output .= '<td>Inactive or Non-regular member</td>';
                        }
                        $output .='</tr>';
                    }
                }
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
        }
    }
    public function beneficiary_search_deceased(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_benefeciaries = DB::table('benefeciaries')
                ->where(function($query) use ($search){
                    $query->where('benefeciaries.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('benefeciaries.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('benefeciaries.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_benefeciaries)
            {
                if(count($data_benefeciaries) >0 ){
                    foreach ($data_benefeciaries as $Benefeciaries) {
                        $output.='<tr>
                            <td><a href="/admin/benefeciaries/'.$Benefeciaries->id.'">'.strtoupper($Benefeciaries->lname).', '.strtoupper($Benefeciaries->fname).' '.strtoupper($Benefeciaries->mname).'</td>';
                        if($Benefeciaries->status == 'active'){
                            $output .='<td><a href="javascript:;" class=" btn btn-info btn-small add_deceased" data-fname="'.$Benefeciaries->fname.'" data-lname="'.$Benefeciaries->lname.'" data-mname="'.$Benefeciaries->mname.'" data-id="'.$Benefeciaries->id.'" data-member_type="beneficiary"><i class="btn-icon-only fas fa-plus"> </i></a></td>';
                        }
                        else{
                            $output .='<td>Inactive Member</td>';
                        }
                            
                        $output .='</tr>';
                    }
                }
                
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
            
        }
    }
    public function member_search_cashier(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_member = DB::table('members')
                ->where(function($query) use ($search){
                    $query->where('members.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_member)
            {
                if(count($data_member) >0 ){
                    foreach ($data_member as $Member) {
                        $output.='<tr>
                            <td><a href="/cashier/member/'.$Member->id.'">'.strtoupper($Member->lname).', '.strtoupper($Member->fname).' '.strtoupper($Member->mname).'</td>';
                        if($Member->status == 'active' && $Member->member_type == 'regular' ){
                            $output .='<td><a href="javascript:;" class=" btn btn-info btn-small add_deceased_cashier" data-fname="'.$Member->fname.'" data-lname="'.$Member->lname.'" data-mname="'.$Member->mname.'" data-id="'.$Member->id.'" data-member_type="member"><i class="btn-icon-only fas fa-plus"> </i></a></td>';
                        }
                        else{
                            $output .= '<td>Inactive or Non-regular member</td>';
                        }
                        $output .='</tr>';
                    }
                }
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
        }
    }
    public function beneficiary_search_deceased__cashier(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_benefeciaries = DB::table('benefeciaries')
                ->where(function($query) use ($search){
                    $query->where('benefeciaries.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('benefeciaries.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('benefeciaries.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_benefeciaries)
            {
                if(count($data_benefeciaries) >0 ){
                    foreach ($data_benefeciaries as $Benefeciaries) {
                        $output.='<tr>
                            <td><a href="/cashier/benefeciaries/'.$Benefeciaries->id.'">'.strtoupper($Benefeciaries->lname).', '.strtoupper($Benefeciaries->fname).' '.strtoupper($Benefeciaries->mname).'</td>';
                        if($Benefeciaries->status == 'active'){
                            $output .='<td><a href="javascript:;" class=" btn btn-info btn-small add_deceased_cashier" data-fname="'.$Benefeciaries->fname.'" data-lname="'.$Benefeciaries->lname.'" data-mname="'.$Benefeciaries->mname.'" data-id="'.$Benefeciaries->id.'" data-member_type="beneficiary"><i class="btn-icon-only fas fa-plus"> </i></a></td>';
                        }
                        else{
                            $output .='<td>Inactive Member</td>';
                        }
                            
                        $output .='</tr>';
                    }
                }
                
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
            
        }
    }
    public function deceased_process(Request $req){
        $data_member = Member::where('member_type', '=', 'regular')->where('status', '!=', 'inactive')->where('id', '!=', $req->id)->get();
        $data_setting = Setting::first();
        $data_deceased = new Deceased();
        $data_deceased->deceased_id = $req->id;
        $data_deceased->amount = $data_setting->standard_fee;
        $data_deceased->amount_received = 0;
        $data_deceased->member_type = $req->member_type;
        $data_deceased->status = 'not-paid';
        $data_deceased->save();
        foreach($data_member  as $Member){
            $data_payment = new Payment();
            $data_payment->member_id = $Member->id;
            $data_payment->deceased_id = $req->id;
            $data_payment->amount = $data_setting->standard_fee;
            $data_payment->user_id = $Member->user_id;
            $data_payment->member_type = $req->member_type;
            $data_payment->payment_type = 'deceased';
            $data_payment->payment_date = date('Y-m-d');
            $data_payment->payment_status = 'not paid';
            $data_payment->save();
        }
        if($req->member_type == 'member'){
            $update_member = Member::where('id', '=', $req->id)
                    ->update(['status' => 'dead']);
        }
        elseif($req->member_type == 'beneficiary'){
            $update_beneficiary = Benefeciary::where('id', '=', $req->id)
                    ->update(['status' => 'dead']);
        }
        
        return response()->json();
    }
    public function deceased_process_cashier(Request $req){
        $data_member = Member::where('member_type', '=', 'regular')->where('status', '!=', 'inactive')->where('id', '!=', $req->id)->get();
        $data_setting = Setting::first();
        $data_deceased = new Deceased();
        $data_deceased->deceased_id = $req->id;
        $data_deceased->amount = $data_setting->standard_fee;
        $data_deceased->amount_received = 0;
        $data_deceased->member_type = $req->member_type;
        $data_deceased->status = 'not-paid';
        $data_deceased->save();
        foreach($data_member  as $Member){
            $data_payment = new Payment();
            $data_payment->member_id = $Member->id;
            $data_payment->deceased_id = $req->id;
            $data_payment->amount = $data_setting->standard_fee;
            $data_payment->user_id = $Member->user_id;
            $data_payment->member_type = $req->member_type;
            $data_payment->payment_type = 'deceased';
            $data_payment->payment_date = date('Y-m-d');
            $data_payment->payment_status = 'not paid';
            $data_payment->save();
        }
        if($req->member_type == 'member'){
            $update_member = Member::where('id', '=', $req->id)
                    ->update(['status' => 'dead']);
        }
        elseif($req->member_type == 'beneficiary'){
            $update_beneficiary = Benefeciary::where('id', '=', $req->id)
                    ->update(['status' => 'dead']);
        }
        
        return response()->json();
    }
    public function view_deceased_cashier($deceased_id){
        $data_deceased = Deceased::where('deceased_id', '=', $deceased_id)->first();
        $data_payment = Payment::with('member')->where('deceased_id', '=', $deceased_id)->get();
        $data_setting = Setting::first();
        if($data_deceased->member_type == 'member'){
            $data_deceased_record = Member::where('id', '=', $data_deceased->deceased_id)->first();
        }
        elseif($data_deceased->member_type == 'beneficiary'){
            $data_deceased_record = Benefeciary::where('id', '=', $data_deceased->deceased_id)->first();
        }
        else {

        }

        return view('cashier.deceased-single', compact('data_deceased_record', 'data_deceased', 'data_payment', 'data_setting'));
    }
    public function view_deceased($deceased_id){
        $data_deceased = Deceased::where('deceased_id', '=', $deceased_id)->first();
        $data_payment = Payment::with('member')->where('deceased_id', '=', $deceased_id)->get();
        $data_setting = Setting::first();
        if($data_deceased->member_type == 'member'){
            $data_deceased_record = Member::where('id', '=', $data_deceased->deceased_id)->first();
        }
        elseif($data_deceased->member_type == 'beneficiary'){
            $data_deceased_record = Benefeciary::where('id', '=', $data_deceased->deceased_id)->first();
        }
        else {

        }

        return view('admin.deceased-single', compact('data_deceased_record', 'data_deceased', 'data_payment', 'data_setting'));
    }
}
