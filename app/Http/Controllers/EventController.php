<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
    //
    public function add_events(Request $req){
        $data_event = new Event();
        $data_event->title = $req->event_title;
        $data_event->message = $req->content;
        $data_event->status = 'active';
        $data_event->save();
        return redirect()->back()->with('success','Event successfully added!');
    }

    public function update_event(Request $req){
    
        $update_event = Event::where('id', '=', $req->id)
                    ->update(['title' => $req->title, 'message' => $req->message]);
        return response()->json();
        
    }
    public function delete_event(Request $req){
        Event::find($req->id)->delete();
        return response()->json();
    }
}
