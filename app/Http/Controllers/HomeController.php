<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Event;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function log_in(){
        return view('auth.login');
    }
    public function thank_you(){
        return view('thank-you');
    }
    public function about(){
        return view('about');
    }
    public function members(){
        $data_member = Member::where('member_type' ,'!=', 'applicant')->latest()->paginate(50);
        return view('members', compact('data_member'));
    }

    public function events(){
        $data_event = Event::latest()->paginate(20);
        return view('events', compact('data_event'));
    }
}
