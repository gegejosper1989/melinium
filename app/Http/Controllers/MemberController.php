<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\User;
use App\Setting;
use App\Benefeciary;
use App\Payment;
use App\Broadcast_Message;
use DB; 
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class MemberController extends Controller
{
    //
    public function index(){
        
        return view('member.home');
    }
    public function members(){
        $data_member = Member::where('member_type' ,'!=', 'applicant')->latest()->paginate(50);
        return view('member.members', compact('data_member'));
    }
    public function view_member($member_id){
        $data_member = Member::with('user')->where('id', '=', $member_id)->first();
        $data_beneficiary = Benefeciary::where('member_id', '=', $member_id)->get();
        return view('member.member', compact('data_member', 'data_beneficiary'));

    }
    public function payments(){
        if (Auth::check()){
            $userId = Auth::user()->id;
        }
        $data_payment = Payment::where('user_id', '=', $userId)->with('member', 'deceased_member', 'deceased_beneficiary')->latest()->paginate(50);
        return view('member.payments', compact('data_payment'));
    }
    public function notifications(){
        if (Auth::check()){
            $userId = Auth::user()->id;
        }
        $data_member = Member::where('user_id', '=', $userId)->first();
        //dd($data_member);
        $data_broadcast = Broadcast_Message::with('broadcast')->where('member_id', '=', $data_member->id)->latest()->paginate(20);
        return view('member.notifications', compact('data_broadcast'));
    }
    public function setting(){
        if (Auth::check()){
            $userId = Auth::user()->id;
        }
        $data_user = User::where('id', '=', $userId)->first();
        return view('member.setting', compact('data_user'));
    }
    public function regular($member_id){
        $update_payment = Member::where('id', '=', $member_id)
                    ->update(['member_type' => 'regular']);
        return redirect('/admin/member/'.$member_id)->with('success','Member successfully updated as Regular Member!');

    }
    public function approve($member_id){
        $update_payment = Member::where('id', '=', $member_id)
                    ->update(['member_type' => 'non-regular']);
        return redirect('/admin/member/'.$member_id)->with('success','Member successfully updated as Non-regular Member!');

    }
    public function cancel($member_id){
        $data_member = Member::where('id', '=', $member_id)->first();
        //User::find($data_member->user_id)->delete();
        Member::find($member_id)->delete();
        return redirect()->back()->with('success','Applicant successfully deleted!');
    }

    public function filter_registration_member($member_type){
        $data_setting = Setting::first();
        $data_member = Member::where('member_type','=',$member_type)->latest()->paginate(50);
        return view('admin.registrations', compact('data_member', 'data_setting'));
    }

    public function filter_member_active($filter_type){
        if($filter_type == 'active'){
            $data_member = Member::where('status', '=', 'active')->latest()->paginate(50);
            return view('admin.members', compact('data_member'));
        }
        elseif($filter_type == 'inactive'){
            $data_member = Member::where('status', '=', 'inactive')->latest()->paginate(50);
            return view('admin.members', compact('data_member'));

        }
        elseif($filter_type == 'regular'){
            $data_member = Member::where('member_type', '=', 'regular')->latest()->paginate(50);
            return view('admin.members', compact('data_member'));

        }
        elseif($filter_type =='non-regular'){
            $data_member = Member::where('member_type', '=', 'non-regular')->latest()->paginate(50);
            return view('admin.members', compact('data_member'));
        }
        else{

        }

    }
    public function members_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_member = DB::table('members')
                ->where(function($query) use ($search){
                    $query->where('members.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('members.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_member)
            {
                if(count($data_member) >0 ){
                    foreach ($data_member as $Member) {
                        
                        $output .='<tr class="row'.$Member->id.'">
                        <td><a href="/member/member/'.$Member->id.'">'.strtoupper($Member->lname).', '.strtoupper($Member->fname).' '.strtoupper($Member->mname).'</a></td>
                        <td>'.$Member->address.'</td>
                        <td>'.$Member->member_type.'</td>
                        <td>'.$Member->status.'</td>
                        <td>
                            <a href="/member/member/'.$Member->id.'" class="btn btn-info btn-small" ><i class="btn-icon-only fas fa-search"> </i></a>
                            
                        </td>
                        </tr>';
                    }
                }
                else {
                    $output.='<tr><td colspan="2" align="center"> <em>No Result</em></td></tr>';
                    
                }
                return Response($output);
            }
        }
    }
    public function save_setting_member(Request $req){
        $update_user = User::where('id', '=', $req->user_id)
                    ->update(['name' => $req->name, 'username' => $req->username, 'email' => $req->email]);

        if(!empty($req->password)){
                $update_pass = User::where('id', '=', $req->user_id)
            ->update(['password' => Hash::make($req->password)]);
        }

        return redirect()->back()->with('success','Account successfully updated!');
        
    }

}
