<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller
{
    //
    public function process_payment(Request $req){
        $data_payment = Payment::where('id', '=', $req->id)->first();
        $update_payment = Payment::where('id', '=', $req->id)
                    ->update(['payment_status' => 'paid', 'payment_date' => $req->payment_date]);
        return response()->json();
        
    }
    public function filter_payments($payment_type){
        $data_payment = Payment::where('payment_type', '=', $payment_type)->with('member', 'deceased_member', 'deceased_beneficiary')->latest()->paginate(50);
        return view('admin.payments', compact('data_payment'));
    }
    public function process_payment_cashier(Request $req){
        $data_payment = Payment::where('id', '=', $req->id)->first();
        $update_payment = Payment::where('id', '=', $req->id)
                    ->update(['payment_status' => 'paid', 'payment_date' => $req->payment_date]);
        return response()->json();
        
    }
    
}
