<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Member;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    //
    public function register_account(Request $req) {
        $data_user = new User();
        $data_user->name = strtoupper($req->lname.' '.$req->fname) ;
        $data_user->username = $req->username;
        $data_user->email = $req->email;
        $data_user->password = Hash::make($req->password);
        $data_user->usertype = 'member';
        $data_user->status = 'applicant';
        $data_user->save();

        $data_member = new Member();
        $data_member->user_id = $data_user->id;
        $data_member->fname = $req->fname;
        $data_member->lname = $req->lname;
        $data_member->mname = '';
        $data_member->dob = $req->dob;
        $data_member->address = $req->address;
        $data_member->gender = $req->gender;
        $data_member->civil_status = $req->civil_status;
        $data_member->profilepic = 'profile.jpg';
        $data_member->status = 'applicant';
        $data_member->save();

        return redirect('/thank-you');
    }
}
