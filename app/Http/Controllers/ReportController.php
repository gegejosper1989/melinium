<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Payment;
use Carbon\Carbon;
class ReportController extends Controller
{
    //
    public function report_deceased(){
        
            $data_member = Member::where('status', '=', 'dead')->latest()->get();
           
            return view('admin.report-deceased', compact('data_member'));
        
    }
    public function report_range_deceased(Request $req){
        $fromdate = Carbon::parse($req->from.' 00:00:00');
        $todate = Carbon::parse($req->to .' 23:59:59'); 
        $data_member = Member::whereBetween('created_at', [$fromdate, $todate])->where('status', '=', 'dead')->latest()->get();
        return view('admin.report-deceased', compact('data_member', 'fromdate', 'todate'));
    
    }
    public function report_range_members(Request $req){
        $fromdate = Carbon::parse($req->from.' 00:00:00');
        $todate = Carbon::parse($req->to .' 23:59:59'); 
        $data_member = Member::whereBetween('created_at', [$fromdate, $todate])->where('member_type', '!=', 'applicant')->latest()->get();
        return view('admin.report-member', compact('data_member', 'fromdate', 'todate'));
    }
    public function report_member(){
        
        $data_member = Member::where('member_type', '!=', 'applicant')->latest()->get();
       
        return view('admin.report-member', compact('data_member'));
    
    }
    public function report_contribution(){
        
        $data_payment = Payment::where('payment_type', '=', 'deceased')->where('payment_status', '=', 'paid')->with('member', 'deceased_member', 'deceased_beneficiary')->latest()->paginate(50);
        return view('admin.report-contribution', compact('data_payment'));
    }
    public function report_range_contribution(Request $req){
        $fromdate = Carbon::parse($req->from);
        $todate = Carbon::parse($req->to); 
        $data_payment = Payment::whereBetween('payment_date', [$fromdate, $todate])->where('payment_type', '=', 'deceased')->where('payment_status', '=', 'paid')->with('member', 'deceased_member', 'deceased_beneficiary')->latest()->paginate(50);
        return view('admin.report-contribution', compact('data_payment', 'fromdate', 'todate'));
    }
}
