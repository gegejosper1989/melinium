<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{

    public function edit_staff(Request $request){

        if($request->ajax()){
            if($request->input('name') == 'password'){
                User::find($request->input('pk'))->update([$request->input('name') => Hash::make($request->input('value'))]);
            }
            else{
                User::find($request->input('pk'))->update([$request->input('name') => $request->input('value')]);
            }
            return response()->json(['success' => true]);
        }
    }
    public function add_staff(Request $req){
        $data_user = new User();
        $data_user->name = strtoupper($req['name']);
        $data_user->username = $req['username'];
        $data_user->email = $req['email'];
        $data_user->password = Hash::make($req['password']);
        $data_user->usertype = $req['usertype'];;
        $data_user->status = 'active';
        $data_user->save();
        return redirect()->back()->with('success','Staff successfully added!');
    }
}
