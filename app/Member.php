<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //
    public function user()
    {
        return $this->belongsTO('App\User', 'user_id', 'id');
    }
}
