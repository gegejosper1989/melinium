<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefeciariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefeciaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('member_id');
            $table->string('fname');
            $table->string('lname');
            $table->string('mname');
            $table->string('dob');
            $table->string('gender');
            $table->string('relationship');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefeciaries');
    }
}
