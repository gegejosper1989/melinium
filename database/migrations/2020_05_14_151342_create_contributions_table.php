<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('deceased_id');
            $table->string('deceased_member_id');
            $table->string('paid');
            $table->string('unpaid');
            $table->string('member_paid');
            $table->string('member_unpaid');
            $table->string('service_fee');
            $table->string('member_type');
            $table->string('member_recievable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributions');
    }
}
