$(document).ready(function() {

    $(document).on('click', '.process-deceased-contribution', function() {
          $('#name').val($(this).data('name'));
          $('#deceased_id').val($(this).data('deceased_id'));
          $('#deceased_member_id').val($(this).data('deceased_member_id'));
          $('#paid').val($(this).data('paid'));
          $('#unpaid').val($(this).data('unpaid'));
          $('#member_paid').val($(this).data('member_paid'));
          $('#member_unpaid').val($(this).data('member_unpaid'));
          var service_fee = $(this).data('member_paid') * $(this).data('service_fee');
          var member_recievable = $(this).data('paid') - service_fee ;
          $('#service_fee').val(service_fee);
          $('#member_type').val($(this).data('member_type'));
          $('#member_recievable').val(member_recievable);
          $('#contribution_modal').modal('show');
         
    });

    $(document).on('click', '.close-success', function() {
        location.reload();
    });

    $('.modal-footer').on('click', '.processcontribution', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/contribution/process',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'deceased_id': $("#deceased_id").val(),
                  'deceased_member_id': $("#deceased_member_id").val(),
                  'paid': $("#paid").val(),
                  'unpaid': $("#unpaid").val(),
                  'member_paid': $("#member_paid").val(),
                  'member_unpaid': $("#member_unpaid").val(),
                  'service_fee': $("#service_fee").val(),
                  'member_type': $("#member_type").val(),
                  'member_recievable': $("#member_recievable").val()
                  
              },
              success: function(data) {
                    $('#success_modal').modal('show');
                }
          });
    });
    $('.modal-footer').on('click', '.processcontribution_cashier', function() {
  
        $.ajax({
            type: 'post',
            url: '/cashier/contribution/process',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'deceased_id': $("#deceased_id").val(),
                'deceased_member_id': $("#deceased_member_id").val(),
                'paid': $("#paid").val(),
                'unpaid': $("#unpaid").val(),
                'member_paid': $("#member_paid").val(),
                'member_unpaid': $("#member_unpaid").val(),
                'service_fee': $("#service_fee").val(),
                'member_type': $("#member_type").val(),
                'member_recievable': $("#member_recievable").val()
                
            },
            success: function(data) {
                  $('#success_modal').modal('show');
              }
        });
  });
});
  