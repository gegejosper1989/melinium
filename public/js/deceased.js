$(document).ready(function() {

    $(document).on('click', '.add_deceased', function() {

          $('#deceased_id').val($(this).data('id'));
          $('#fname').val($(this).data('fname'));
          $('#lname').val($(this).data('lname'));
          $('#mname').val($(this).data('mname'));
          $('#member_type').val($(this).data('member_type'));
          
          $('#confirm_modal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
    });
    
  
    $('.modal-footer').on('click', '.processdead', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/deceased/process',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#deceased_id").val(),
                  'member_type': $('#member_type').val(),
              },
              success: function(data) {
                  alert('Member successfully declare as dead.');
                  location.reload();
                }
          });
    });

    $(document).on('click', '.add_deceased_cashier', function() {

        $('#deceased_id').val($(this).data('id'));
        $('#fname').val($(this).data('fname'));
        $('#lname').val($(this).data('lname'));
        $('#mname').val($(this).data('mname'));
        $('#member_type').val($(this).data('member_type'));
        
        $('#confirm_modal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
  });
  

  $('.modal-footer').on('click', '.processdead_cashier', function() {

        $.ajax({
            type: 'post',
            url: '/cashier/deceased/process',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'id': $("#deceased_id").val(),
                'member_type': $('#member_type').val(),
            },
            success: function(data) {
                alert('Member successfully declare as dead.');
                location.reload();
              }
        });
  });
});
  