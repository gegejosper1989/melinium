$(document).ready(function() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
    $(document).on('click', '.edit-content', function() {

          $('#edit_id').val($(this).data('id'));
          $('#edit_title').val($(this).data('title'));
          $('#edit_content').val($(this).data('message'));
          
          $('#content_modal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
    });
    
      $(document).on('click', '.delete-content', function() {

          $('#delete_id').val($(this).data('id'));
          $('#delete_modal').modal('show');
      });
      $(document).on('click', '.close-success', function() {
        location.reload();
    });
  
      $('.modal-footer').on('click', '.edit', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/event/edit',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#edit_id").val(),
                  'title': $('#edit_title').val(),
                  'message': $('#edit_content').val()
              },
                success: function(data) {
                
                $('#success_modal').modal('show');
                }
          });
      });



      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/event/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#delete_id').val()
              },
              success: function(data) {
                $('#success_modal').modal('show');
            }
          });
      });
  });
  