$(document).ready(function() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
    $(document).on('click', '.edit-member', function() {

          $('#edit_id').val($(this).data('id'));
          $('#edit_fname').val($(this).data('fname'));
          $('#edit_lname').val($(this).data('lname'));
          $('#edit_mname').val($(this).data('mname'));
          $('#edit_dob').val($(this).data('dob'));
          $('#edit_address').val($(this).data('address'));
          $('#edit_gender').val($(this).data('gender'));
          $('#edit_contact_number').val($(this).data('contact_number'));
          $('#member_modal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
    });
    $(document).on('click', '.edit-beneficiary', function() {

        $('#edit_beneficiary_id').val($(this).data('id'));
        $('#edit_beneficiary_fname').val($(this).data('fname'));
        $('#edit__beneficiary_lname').val($(this).data('lname'));
        $('#edit_beneficiary_mname').val($(this).data('mname'));
        $('#edit_beneficiary_dob').val($(this).data('dob'));
        $('#edit_beneficiary_relationship').val($(this).data('relationship'));
        $('#edit_beneficiary_gender').val($(this).data('gender'));
        $('#beneficiary_modal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });
      $(document).on('click', '.delete-modal', function() {

          $('#delete_id').val($(this).data('id'));
          $('#delete_modal').modal('show');
      });
  
      $('.modal-footer').on('click', '.edit', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/member/edit',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#edit_id").val(),
                  'fname': $('#edit_fname').val(),
                  'lname': $('#edit_lname').val(),
                  'mname': $('#edit_mname').val(),
                  'dob': $('#edit_dob').val(),
                  'member_type': $('#edit_member_type').val(),
                  'contact_number': $('#edit_contact_number').val(),
                  'address': $('#edit_address').val(),
                  'gender': $('input[name=edit_gender]').val(),
                  'member_type': $('select[name=edit_member_type]').val(),
                  'civil_status': $('select[name=edit_civil_status]').val()
              },
              success: function(data) {
                  alert('Edit Success');
                  location.reload();
                }
          });
      });

        $('.modal-footer').on('click', '.update_beneficiary', function() {
  
            $.ajax({
                type: 'post',
                url: '/admin/beneficiary/edit',
                data: {
                    //_token:$(this).data('token'),
                    '_token': $('input[name=_token]').val(),
                    'id': $("#edit_beneficiary_id").val(),
                    'fname': $('#edit_beneficiary_fname').val(),
                    'lname': $('#edit__beneficiary_lname').val(),
                    'mname': $('#edit_beneficiary_mname').val(),
                    'dob': $('#edit_beneficiary_dob').val(),
                    'gender': $('input[name=edit_beneficiary_gender]').val(),
                    'relationship': $('select[name=edit_beneficiary_relationship]').val()
                },
                success: function(data) {
                    alert('Edit Success');
                    location.reload();
                }
            });
        });

      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/member/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#delete_id').val()
              },
              success: function(data) {
               
                  $('.row' + $('#delete_id').val()).remove();
                  toastr.error('Member successfull deleted!')
            }
          });
      });
  });
  