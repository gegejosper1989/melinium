$(document).ready(function() {

    $(document).on('click', '.pay-member', function() {

          $('#payment_id').val($(this).data('id'));
          $('#fname').val($(this).data('fname'));
          $('#lname').val($(this).data('lname'));
          $('#mname').val($(this).data('mname'));
          $('#dfname').val($(this).data('dfname'));
          $('#dlname').val($(this).data('dlname'));
          $('#dmname').val($(this).data('dmname'));
          $('#amount').val($(this).data('amount'));
          $('#payment_modal').modal('show');
         
    });

    $(document).on('click', '.close-success', function() {
        location.reload();
    });

    $('.modal-footer').on('click', '.processpayment', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/payment/process',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#payment_id").val(),
                  'payment_date': $('#payment_date').val(),
                  'amount': $('#amount').val()
              },
              success: function(data) {
                    $('#success_modal').modal('show');
                }
          });
    });

    $('.modal-footer').on('click', '.processpayment-cashier', function() {
  
        $.ajax({
            type: 'post',
            url: '/cashier/payment/process',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'id': $("#payment_id").val(),
                'payment_date': $('#payment_date').val(),
                'amount': $('#amount').val()
            },
            success: function(data) {
                  $('#success_modal').modal('show');
              }
        });
  });

    
});
  