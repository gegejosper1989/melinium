@extends('layouts.page')

@section('content')
<!-- BEGIN SERVICE BOX -->   
<div class="row service-box margin-bottom-40">
<div class="col-md-12 col-sm-12">
    <h1>About Us</h1>
    <div class="content-page">
        <div class="row margin-bottom-30">
        <!-- BEGIN INFO BLOCK -->               
        <div class="col-md-10">
            <h2 class="no-top-space text-center">Rules And Regulation</h2>
            <p>Kining maong kapunungan gitoyo pagtukod sa mga opisyalis sa purok MILLIENIUM. Sa tumong nga danali-ang tabang ngadto sa miyembro nga mo panaw ngadto; sa laing kalibotan kon tawagon na siya sa kmtayon. Ug kining maong kapunungan, gipahuman ug mg balaod nga angy gayod ipatuman diha ang mga balod nga gikasabutan sa mg opiyalis s purok ug ma membro..</p> 
            <p>KASABUTAN:</p>
            <!-- BEGIN LISTS -->
            <div class="row front-lists-v1">
            <div class="col-md-6">
                <ul class="list-unstyled margin-bottom-20">
                <li><i class="fa fa-check"></i> Obligasyon nga P105 ang pundo kon gusto moapil.</li>
                <li><i class="fa fa-check"></i> Kinahanglan klarohon gayud ang paglista ang member sa maong kapunungan. </li>
                <li><i class="fa fa-check"></i> Kinahanglan nga kon ato man sakopon ang atong inahan ug amahan nga biyodo ug biyuda, kinahanglan gyud nga naa sa atoang kamot.</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list-unstyled">
                <li><i class="fa fa-check"></i> Death Certificate</li>
                <li><i class="fa fa-check"></i> Kinahanglan nga ang i-apil nga anak sa original nga membro, walay labot o kalbi.in.</li>
                <li><i class="fa fa-check"></i> 60 years old below mag ihap siya ug 30 days ayha siya ma regular member. And 60 pataas mag ihap siya 90 days ayha siya ma regular member. </li>
                </ul>
                
            </div>
            
            </div>
            <!-- END LISTS -->
            <p>Dayon kung mapakyas ka sa pagbayad sa lim k adlaw  gikan sa pagkhitabo, mobalik ka pgka new member. Moihap ka ug 30 days usa ka maregular member ug usab . Sa panahong nga suspended ka sa maong kapunungng waalay tulobagon o bayronon kon adunay nahitabo sa pamilya nga imong gipamembro  </p>
        </div>
        <!-- END INFO BLOCK -->   

        </div>

        
    </div>
    </div>
</div>
@endsection
