@extends('layout.admin')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Claims</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Claims</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                    Session::forget('success');
                    @endphp
                </div>
            @endif

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Claimed List</h3>
                
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              
              
              {{ $data_deceased->links() }}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Date</th>
                      <th>Name</th>
                      <th>Amount Received</th>
                      <th>Member Type</th>
                      
                     
                    </tr>
                  </thead>
                  <tbody class="deceasedresult">
                    @forelse($data_deceased as $Deceased)
                    <tr class="row{{$Deceased->id}}">
                      <td>{{$Deceased->updated_at->format('d-m-Y')}}</td>
                      <td>
                            @if($Deceased->member_type == 'member')
                                <a href="/admin/deceased/{{$Deceased->deceased_id}}"> {{strtoupper($Deceased->deceased_member->lname)}}, {{strtoupper($Deceased->deceased_member->fname)}} {{strtoupper($Deceased->deceased_member->mname)}} </a>
                                    
                            @elseif($Deceased->member_type == 'beneficiary')
                                <a href="/admin/deceased/{{$Deceased->deceased_id}}"> {{strtoupper($Deceased->deceased_beneficiary->lname)}}, {{strtoupper($Deceased->deceased_beneficiary->fname)}} {{strtoupper($Deceased->deceased_beneficiary->mname)}} </a>
                            @else
                            @endif    
                      
                      <td>{{number_format($Deceased->amount_received,2)}}</td>
                      <td>{{$Deceased->member_type}}</td>
                      
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div class="modal fade" id="payment_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Contribution</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h5>Member</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Last Name">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" readonly>
                    </div>
                </div>
            </div>
            <hr>
            <h5>Deceased</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" class="form-control" id="dfname" name="dfname" placeholder="First Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" class="form-control" id="dmname" name="dmname" placeholder="Middle Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Last Name">Last Name</label>
                    <input type="text" class="form-control" id="dlname" name="dlname" placeholder="Last Name" readonly>
                    </div>
                </div>
            </div>
            <hr>
            <h5>Contribution</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Amount">Amount</label>
                    <input type="text" class="form-control" id="amount" name="amount">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Date">Date</label>
                    <input type="date" class="form-control" id="payment_date" name="payment_date" value="{{date('Y-m-d')}}">
                    </div>
                </div>
                
            </div>
            <input type="hidden" class="form-control" id="payment_id" name="payment_id">
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success processpayment" data-dismiss="modal">Process Payment</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="success_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Process success!</h4>
            <button type="button" class="close close-success" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Member successfully paid it's contribution.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success text-right close-success" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/payments_search')}}',
    data:{'search':$value},
    success:function(data){
      $('.paymentresult').html(data);
    } 
  });
})
</script> 
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/payment.js') }}"></script>
@endsection