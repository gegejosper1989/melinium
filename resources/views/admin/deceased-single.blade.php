@extends('layout.admin')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Deceased</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Member</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-12 col-lg-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Decease Details</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body p-0">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{asset('img/profile.png')}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{strtoupper($data_deceased_record->lname)}}, {{strtoupper($data_deceased_record->fname)}} {{strtoupper($data_deceased_record->mname)}} </h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Member Type</b> <a class="float-right">{{$data_deceased->member_type}}</a>
                  </li>
                    <?php
                    $total_payment_paid = 0;
                    $total_payment_unpaid = 0;
                    $total_member_paid = 0;
                    $total_member_unpaid = 0;
                    ?>
                    @foreach($data_payment as $Payment)
                    <?php 
                    
                    if($Payment->payment_status == 'paid'){
                        $total_payment_paid = $total_payment_paid + $Payment->amount;
                        $total_member_paid = $total_member_paid + 1;
                    }
                    else {
                        $total_payment_unpaid = $total_payment_unpaid + $Payment->amount;
                        $total_member_unpaid = $total_member_unpaid + 1;
                    }
                    ?>
                    @endforeach
                    <li class="list-group-item">
                    <b>Total Members Paid</b> <a class="float-right">{{$total_member_paid}}</a>
                    </li>
                    <li class="list-group-item">
                    <b>Total Members Unpaid</b> <a class="float-right">{{$total_member_unpaid}}</a>
                    </li>
                    <li class="list-group-item">
                    <b>Total Amount Paid</b> <a class="float-right">{{number_format($total_payment_paid,2)}}</a>
                    </li>
                    <li class="list-group-item">
                    <b>Total Amount Unpaid</b> <a class="float-right">{{number_format($total_payment_unpaid,2)}}</a>
                    </li>
                    
                    @if($data_deceased->status != 'paid')
                    <li class="list-group-item text-center">
                    <a href="javascript:;" class="process-deceased-contribution btn btn-sm btn-success"
                        data-deceased_id="{{$data_deceased->id}}"
                        data-deceased_member_id="{{$data_deceased->deceased_id}}"
                        data-paid="{{$total_payment_paid}}"
                        data-member_paid="{{$total_member_paid}}"
                        data-member_unpaid="{{$total_member_unpaid}}"
                        data-unpaid="{{$total_payment_unpaid}}"
                        data-name = "{{strtoupper($data_deceased_record->lname)}}, {{strtoupper($data_deceased_record->fname)}} {{strtoupper($data_deceased_record->mname)}}"
                        data-service_fee = "{{$data_setting->service_fee}}"
                        data-member_type = "{{$data_deceased->member_type}}"
                    >
                        <i class="fas fa-save"> </i> Process Contribution
                    </a>
                    @else
                    <li class="list-group-item">
                    <b>Contribution Recieved</b> <a class="float-right">{{number_format($data_deceased->amount_received,2)}}</a>
                    </li>
                    @endif
                 
                
                  </li>
                </ul>
              </div>
             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Contributions</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              
                <table class="table table-striped">
                  <thead>
                    <tr> 
                      <th>Member</th>
                      <th>Member Type</th>
                      <th>Amount</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_payment as $Payment)
                    
                    <tr>
                     <td>{{strtoupper($Payment->member->lname)}}, {{strtoupper($Payment->member->fname)}} {{strtoupper($Payment->member->mname)}} </td>
                     <td>{{$Payment->member->member_type}}</td>
                     <td>{{number_format($Payment->amount, 2)}}</td>
                     <td>{{$Payment->payment_status}}</td>
                     <td></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
                <div style="margin:20px;">
                  <button class="btn btn-info btn-sm no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                     
                </div>
            </div>
            <!-- /.card -->

            
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
   
<div class="modal fade" id="contribution_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Process Contribution</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h5>Deceased Member</h5>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                    <label for="Name">Name</label>
                    <input type="text" class="form-control" id="name" name="name"  readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Member Type">Member Type</label>
                    <input type="text" class="form-control" id="member_type" name="member_type"  readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="Paid Member">Paid Member</label>
                    <input type="text" class="form-control" id="member_paid" name="member_paid" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="Unpaid Member">Unpaid Member</label>
                    <input type="text" class="form-control" id="member_unpaid" name="member_unpaid" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="Amount Member Paid">Amount Member Paid</label>
                    <input type="text" class="form-control" id="paid" name="paid" readonly>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="Amount Member Unpaid">Amount Member Unpaid</label>
                    <input type="text" class="form-control" id="unpaid" name="unpaid" readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="Service Fee">Service Fee</label>
                    <input type="text" class="form-control" id="service_fee" name="service_fee" readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="Member Recievable">Member Recievable </label>
                    <input type="text" class="form-control" id="member_recievable" name="member_recievable" readonly>
                    </div>
                </div>
            </div>
            <hr>

            <input type="hidden" class="form-control" id="deceased_id" name="deceased_id">
            <input type="hidden" class="form-control" id="deceased_member_id" name="deceased_member_id">
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success processcontribution" data-dismiss="modal">Process Contribution</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="success_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Process success!</h4>
            <button type="button" class="close close-success" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Contribution successfully processed!.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success text-right close-success" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/contribution.js') }}"></script>
@endsection