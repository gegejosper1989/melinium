@extends('layout.admin')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Members</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Members</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Members List</h3>
                    
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="input-group">
                            <input id="search" type="text" type="search" name="search" class="form-control" placeholder="Search Member">
                            {{ csrf_field() }}
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Name</th> 
                                </tr>
                            </thead>
                            <tbody class="memberresult">
                                
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Benefeciary List</h3>
                    
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="input-group">
                            <input id="search_beneficiary" type="text" type="search" name="search" class="form-control" placeholder="Search Member">
                            {{ csrf_field() }}
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Name</th> 
                                </tr>
                            </thead>
                            <tbody class="beneficiaryresult">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          <div class="col-md-8">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                    Session::forget('success');
                    @endphp
                </div>
            @endif

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Recent Deceased Members</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              {{ $data_member->links() }}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Name</th>
                      <th>Address</th>
                      <th>Member Type</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($data_member as $Member)
                    <tr class="row{{$Member->id}}">
                      <td><a href="/admin/deceased/{{$Member->id}}">{{strtoupper($Member->lname)}}, {{strtoupper($Member->fname)}} {{strtoupper($Member->mname)}} </a></td>
                      <td>{{$Member->address}}</td>
                      <td>{{$Member->member_type}}</td>
                      <td>{{$Member->status}}</td>
                      <td>
                          <a href="/admin/deceased/{{$Member->id}}" class="btn btn-info btn-small" ><i class="btn-icon-only fas fa-search"> </i></a>
                         
                          <!-- <a href="javascript:;" class="delete-modal btn btn-danger btn-small" data-id="{{$Member->id}}"><i class="btn-icon-only fas fa-times"> </i></a> -->
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Recent Deceased Benefeciaries</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              {{ $data_member->links() }}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Name</th>
                      <th>Member</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($data_beneficiary as $Beneficiary)
                    <tr class="row{{$Beneficiary->id}}">
                      <td><a href="/admin/deceased/{{$Beneficiary->id}}">{{strtoupper($Beneficiary->lname)}}, {{strtoupper($Beneficiary->fname)}} {{strtoupper($Beneficiary->mname)}} </a></td>
                      <td>{{strtoupper($Beneficiary->member->lname)}}, {{strtoupper($Beneficiary->member->fname)}} {{strtoupper($Beneficiary->member->mname)}}</td>
                      
                      <td>{{$Beneficiary->status}}</td>
                      <td>
                          <a href="/admin/deceased/{{$Beneficiary->id}}" class="btn btn-info btn-small" ><i class="btn-icon-only fas fa-search"> </i></a>
                       
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div class="modal fade" id="confirm_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Are you sure you want to declare this member as dead?</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="Last Name">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="Member Type">Member Type</label>
                    <input type="text" class="form-control" id="member_type" name="member_type" readonly>
                    </div>
                </div>
            </div>
            <input type="hidden" class="form-control" id="deceased_id" name="deceased_id">
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger processdead" data-dismiss="modal">Process</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="success_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Process success!</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Member successfully declare as dead.
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/member_search_deceased')}}',
    data:{'search':$value},
    success:function(data){
      $('.memberresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$('#search_beneficiary').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/beneficiary_search_deceased')}}',
    data:{'search':$value},
    success:function(data){
      $('.beneficiaryresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/deceased.js') }}"></script>
@endsection