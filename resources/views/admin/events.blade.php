@extends('layout.admin')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Events</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Events</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-4">
              <div class="card">
                  <div class="card-header">
                      <h3 class="card-title">Events List</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                      <form action="{{route('add_events')}}" method="post">
                        <div class="form-group">
                          <label for="Content">Event Title</label>
                            <input id="event_title" type="text" name="event_title" class="form-control" placeholder="Event Title">
                            {{ csrf_field() }}
                        </div>

                        <div class="form-group">
                            <label for="Content">Content</label>
                            <textarea rows="10" cols="20" name="content" id="content" class="form-control">
                            </textarea>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-save"></i> Save</button>
                      </form>
                  </div>
              </div>  
          </div>
          <div class="col-md-8">
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-calendar"></i>
                  Events
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                        Session::forget('success');
                        @endphp
                    </div>
                @endif
                {{$data_event->links()}}
                @forelse($data_event as $Event)
                  <div class="callout callout-success">
                    <h5>{{$Event->title}}</h5>

                    <p>{{$Event->message}}</p>
                    <div class="text-right">
                    <a href="javascript:;" 
                      data-id="{{$Event->id}}"
                      data-title="{{$Event->title}}"
                      data-message="{{$Event->message	}}"
                      class="btn btn-info btn-sm edit-content"><i class="btn-icon-only fas fa-pen"></i></a>
                    <a href="javascript:;" 
                    data-id="{{$Event->id}}"
                    class="btn btn-danger btn-sm delete-content"><i class="btn-icon-only fas fa-times"></i></a>
                    </div> 
                  </div>
                  @empty
                  <em>No Events</em>
                  @endforelse
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div id="content_modal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Event</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="First Name">Event Title</label>
              <input type="text" class="form-control" id="edit_title" name="edit_title" required>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="Content">Content</label>
              <textarea name="edit_content" id="edit_content" cols="30" rows="10" class="form-control"></textarea>
            </div>
          </div>
          
        </div>

      <div class="modal-footer justify-content-between">
        <input type="hidden" class="form-control" id="edit_id" name="edit_id">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary edit" data-dismiss="modal">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</div>
<div class="modal fade" id="delete_modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete Event</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Event?</p>
        <input type="hidden" class="form-control" id="delete_id" name="delete_id">
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger delete" data-dismiss="modal">Delete</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="success_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update success!</h4>
            <button type="button" class="close close-success" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Event successfully updated.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success text-right close-success" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/event.js') }}"></script>
@endsection