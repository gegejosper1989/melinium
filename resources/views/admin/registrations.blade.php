@extends('layout.admin')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Registrations</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Registrations</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                    Session::forget('success');
                    @endphp
                </div>
            @endif

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Members List</h3>
                
                <div class="text-right">
                  <a href="/admin/registrations/filter/non-regular" class="btn btn-info btn-sm"> Non-regular</a> <a href="/admin/registrations/filter/applicant" class="btn btn-danger btn-sm"> Applicants</a> 
                </div>
                
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              
              @if(Session::has('warning'))
                  <div class="alert alert-warning">
                      {{ Session::get('warning') }}
                      @php
                      Session::forget('warning');
                      @endphp
                  </div>
              @endif
              {{ $data_member->links() }}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Full Name</th>
                      
                      <th>Member Type</th>
                      <th>Age</th>
                      <th>Probation Days</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody class="memberresult">
                    @forelse($data_member as $Member)
                    <tr class="row{{$Member->id}}">
                      <td><a href="/admin/member/{{$Member->id}}">{{strtoupper($Member->lname)}}, {{strtoupper($Member->fname)}} {{strtoupper($Member->mname)}} </a></td>
                      <td>{{$Member->member_type}}</td>
                      <?php 
                      
                      $created = new \Carbon\Carbon ($Member->created_at);
                      $now = \Carbon\Carbon::now();
                      $difference = ($created->diff($now)->days < 1)
                          ? 0
                          : $created->diffInDays($now);
                      $age = \Carbon\Carbon::parse($Member->dob)->diff(\Carbon\Carbon::now())->format('%y years old');
                      $agecount = \Carbon\Carbon::parse($Member->dob)->diff(\Carbon\Carbon::now())->format('%y');
                      if($agecount < $data_setting->from_age){
                        $probation_days = $data_setting->standard_duration;  
                      }
                      else {
                        $probation_days = $data_setting->duration;  
                      } 
                      ?>
                      <td>{{$age}}</td>
                      <td>{{$difference}} days</td>
                      <td>
                          
                          @if($Member->member_type == 'non-regular')
                            @if($difference >=$probation_days)                        
                            <a href="/admin/member/regular/{{$Member->id}}" class="btn btn-success btn-small" ><i class="btn-icon-only fas fa-plus"> </i> Regular</a>
                            @else 
                          
                            <a href="/admin/member/regular/{{$Member->id}}" class="btn btn-success btn-small" style="pointer-events: none; opacity: .5;"><i class="btn-icon-only fas fa-plus"> </i> Regular</a> 
                            @endif 
                          @endif 
                          @if($Member->member_type == 'applicant')
                          <a href="/admin/member/approve/{{$Member->id}}" class="btn btn-info btn-small" ><i class="btn-icon-only fas fa-check"> </i> Approved</a>
                          <a href="/admin/member/cancel/{{$Member->id}}" class="btn btn-danger btn-small" ><i class="btn-icon-only fas fa-times"> </i> Cancel</a>
                          @endif
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div id="member_modal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Member</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="First Name">First Name</label>
              <input type="text" class="form-control" id="edit_fname" name="edit_fname" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Middle Name">Middle Name</label>
              <input type="text" class="form-control" id="edit_mname" name="edit_mname" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Last Name">Last Name</label>
              <input type="text" class="form-control" id="edit_lname" name="edit_lname" required>
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="Birth Date">Birth Date</label>
              <input type="date" class="form-control" id="edit_dob" name="edit_dob" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Gender">Gender</label>
                <div class="row">
                  <div class="form-check col-md-6  pl-5">
                    <input class="form-check-input" type="radio" name="edit_gender" value="male" required>
                    <label class="form-check-label">Male</label>
                  </div>
                  <div class="form-check col-md-6 text-left">
                    <input class="form-check-input" type="radio" name="edit_gender" value="female" required>
                    <label class="form-check-label">Female</label>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Civil Status">Civil Status</label>
                <select class="form-control" name="edit_civil_status">
                    <option>Single</option>
                    <option>Married</option>
                    <option>Separated</option>
                    <option>Widowed</option>
                </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="Contact Number">Contact Number</label>
              <input type="number" class="form-control" id="edit_contact_number" name="edit_contact_number" placeholder="Contact Number" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Member Type">Member Type</label>
              <select class="form-control" name="edit_member_type">
                  <option>non-regular</option>
                  <option>regular</option>
              </select>
            </div>
          </div>  
          <div class="col-md-4">
            <div class="form-group">
              <label for="Address">Address</label>
              <input type="text" class="form-control" id="edit_address" name="edit_address" placeholder="Address" value="{{ old('address') }}" required>
            </div>
          </div>  
        </div>
      <div class="modal-footer justify-content-between">
        <input type="hidden" class="form-control" id="edit_id" name="edit_id">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary edit" data-dismiss="modal">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</div>
<div class="modal fade" id="delete_modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete Member</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Member?</p>
        <input type="hidden" class="form-control" id="delete_id" name="delete_id">
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger delete" data-dismiss="modal">Delete</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/members_search')}}',
    data:{'search':$value},
    success:function(data){
      $('.memberresult').html(data);
    } 
  });
})
</script> 
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/member.js') }}"></script>
@endsection