@extends('layout.admin')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Contributions</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Contributions</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-12">
        

            <div class="card">
              <div class="card-header">
                <form action="{{route('report_range_contribution')}}" method="post">
                    @csrf
                    <div class="input-group">
                        <div class="col-lg-3"><input type="date" class="form-control float-right" id="from" name="from"></div>
                        <div class="col-lg-3"><input type="date" class="form-control float-left" id="to" name="to"></div>
                        <div class="col-lg-3"><button type="submit" class="btn btn-sm btn-success"><i class="fas fa-search"></i> Search</button></div>
                    </div>
                </form>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(isset($fromdate) && isset($todate))
                    <strong>Contribution from : {{$fromdate->format('m-d-Y')}} - {{$todate->format('m-d-Y')}}</strong>
                @endif
              {{ $data_payment->links() }}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Date</th>
                      <th>Name</th>
                      
                      <th>Deceased</th>
                      <th>Amount</th>      
                    </tr>
                  </thead>
                  <tbody class="paymentresult">
                      <?php $total_amount = 0; ?>
                    @forelse($data_payment as $Payment)
                    <tr class="row{{$Payment->id}}">
                      <td>{{$Payment->created_at->format('d-m-Y')}}</td>
                      <td>{{strtoupper($Payment->member->lname)}}, {{strtoupper($Payment->member->fname)}} {{strtoupper($Payment->member->mname)}} </td>
                      <td>
                        @if($Payment->deceased_id == 0)
                            N/A
                        @else
                            @if($Payment->member_type == 'member' && $Payment->payment_type == 'deceased')
                                {{strtoupper($Payment->deceased_member->lname)}}, {{strtoupper($Payment->deceased_member->fname)}} {{strtoupper($Payment->deceased_member->mname)}}   
                            @elseif($Payment->member_type == 'beneficiary' && $Payment->payment_type == 'deceased')
                                {{strtoupper($Payment->deceased_beneficiary->lname)}}, {{strtoupper($Payment->deceased_beneficiary->fname)}} {{strtoupper($Payment->deceased_beneficiary->mname)}}
                            @else 
                            @endif
                        @endif
                      </td>
                      <td>{{number_format($Payment->amount,2)}}</td>
                      <?php $total_amount = $total_amount + $Payment->amount; ?>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    
                    @endforelse
                    <tr>
                        <td colspan="3" class="text-right"><strong>Total</strong></td>
                        <td><strong>P {{number_format($total_amount,2)}}</strong></td>
                    </tr>
                  </tbody>
                </table>
                <div style="margin:20px;">
                  <button class="btn btn-info btn-sm no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div class="modal fade" id="payment_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Contribution</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h5>Member</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Last Name">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" readonly>
                    </div>
                </div>
            </div>
            <hr>
            <h5>Deceased</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" class="form-control" id="dfname" name="dfname" placeholder="First Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" class="form-control" id="dmname" name="dmname" placeholder="Middle Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Last Name">Last Name</label>
                    <input type="text" class="form-control" id="dlname" name="dlname" placeholder="Last Name" readonly>
                    </div>
                </div>
            </div>
            <hr>
            <h5>Contribution</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Amount">Amount</label>
                    <input type="text" class="form-control" id="amount" name="amount">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Date">Date</label>
                    <input type="date" class="form-control" id="payment_date" name="payment_date" value="{{date('Y-m-d')}}">
                    </div>
                </div>
                
            </div>
            <input type="hidden" class="form-control" id="payment_id" name="payment_id">
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success processpayment" data-dismiss="modal">Process Payment</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="success_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Process success!</h4>
            <button type="button" class="close close-success" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Member successfully paid it's contribution.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success text-right close-success" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/payments_search')}}',
    data:{'search':$value},
    success:function(data){
      $('.paymentresult').html(data);
    } 
  });
})
</script> 
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/payment.js') }}"></script>
@endsection