@extends('layout.admin')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Report Deceased </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Report Deceased</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <form action="{{route('report_range_deceased')}}" method="post">
                    @csrf
                    <div class="input-group">
                        <div class="col-lg-3"><input type="date" class="form-control float-right" id="from" name="from"></div>
                        <div class="col-lg-3"><input type="date" class="form-control float-left" id="to" name="to"></div>
                        <div class="col-lg-3"><button type="submit" class="btn btn-sm btn-success"><i class="fas fa-search"></i> Search</button></div>
                    </div>
                </form>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(isset($fromdate) && isset($todate))
                    <strong>Deceased from : {{$fromdate->format('m-d-Y')}} - {{$todate->format('m-d-Y')}}</strong>
                @endif
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Name</th>
                      <th>Address</th>
                      <th>Member Type</th>
                      <th>Status</th>
                     
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($data_member as $Member)
                    <tr class="row{{$Member->id}}">
                      <td>{{strtoupper($Member->lname)}}, {{strtoupper($Member->fname)}} {{strtoupper($Member->mname)}}</td>
                      <td>{{$Member->address}}</td>
                      <td>{{$Member->member_type}}</td>
                      <td>{{$Member->status}}</td>
                      
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
                <div style="margin:20px;">
                  <button class="btn btn-info btn-sm no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>     
                </div>
            </div>
            <!-- /.card -->
           
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection