@extends('layout.admin')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Setting</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Setting</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          

        <div class="col-md-12">
        @if(Session::has('success'))
              <div class="alert alert-success">
                  {{ Session::get('success') }}
                  @php
                  Session::forget('success');
                  @endphp
              </div>
          @endif
            <form action="{{route ('settings_update') }}" method="POST">
              @csrf
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Settings</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="From Age">From Age+</label>
                        <input type="number" class="form-control" id="from_age" name="from_age" placeholder="From Age" value="{{$data_setting->from_age}}" required>
                      </div>
                    </div>
                   
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Duration">Age+ Duration (Days)</label>
                        <input type="number" class="form-control" id="duration" name="duration" placeholder="Number of Days" value="{{$data_setting->duration}}" required>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Duration">Standard Duration (Days)</label>
                        <input type="number" class="form-control" id="standard_duration" name="standard_duration" placeholder="Number of Days Standard" value="{{$data_setting->standard_duration}}" required>
                      </div>
                    </div>
                    
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Registration Fee">Registration Fee</label>
                        <input type="number" class="form-control" id="registration_fee" name="registration_fee" placeholder="Registration Fee" value="{{$data_setting->registration_fee}}" required>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Registration Fee">Contribution Fee</label>
                        <input type="number" class="form-control" id="standard_fee" name="standard_fee" placeholder="Contribution Fee" value="{{$data_setting->standard_fee}}" required>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Limit Days">Limit Days</label>
                        <input type="number" class="form-control" id="limit_days" name="limit_days" placeholder="Limit Days" value="{{$data_setting->limit_days}}" required>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" class="form-control" id="setting_id" name="setting_id" value="{{$data_setting->id}}">
                  <button type="submit" class="btn btn-md btn-info"> <i class="fas fa-save"></i> Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection