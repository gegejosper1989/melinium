@extends('layouts.page')

@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
        @php
        Session::forget('success');
        @endphp
    </div>
@endif
<h1>Register your account</h1>
<div class="content-form-page">
    <div class="row">
    <div class="col-md-7 col-sm-7">
    <form method="POST" action="{{ route('register') }}">
    @csrf
        <fieldset>
            <legend>Your personal details</legend>
            <div class="form-group">
            <label for="firstname" class="col-lg-4 control-label">First Name <span class="require">*</span></label>
            <div class="col-lg-8">
                <input type="text" class="form-control" id="fname" name="fname">
            </div>
            </div>
            <div class="form-group">
                <label for="lastname" class="col-lg-4 control-label">Last Name <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="lname" name="lname">
                </div>
            </div>
            <div class="form-group">
                <label for="Birth Date" class="col-lg-4 control-label">Birth Date <span class="require">*</span></label>
                <div class="col-lg-8">
                <input type="date" class="form-control" id="dob" name="dob" required>
                </div>
            </div>
            <div class="form-group">
                <label for="Gender" class="col-lg-4 control-label">Gender <span class="require">*</span></label>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="form-check col-md-6  pl-5">
                        <input class="form-check-input" type="radio" name="gender" value="male" required>
                        <label class="form-check-label">Male</label>
                        </div>
                        <div class="form-check col-md-6 text-left">
                        <input class="form-check-input" type="radio" name="gender" value="female" required>
                        <label class="form-check-label">Female</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="Civil Status" class="col-lg-4 control-label">Civil Status <span class="require">*</span></label>
                <div class="col-lg-8">
                <select class="form-control" name="civil_status">
                              <option>Single</option>
                              <option>Married</option>
                              <option>Separated</option>
                              <option>Widowed</option>
                          </select>
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-lg-4 control-label">Address<span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="address" name="address">
                </div>
            </div>
            <div class="form-group">
                <label for="Contact Number" class="col-lg-4 control-label">Contact Number<span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="+63990-000-0000">
                </div>
            </div>
            <div class="form-group">
            <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
            <div class="col-lg-8">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Login Details</legend>
            <div class="form-group">
                <label for="username" class="col-lg-4 control-label">Username <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username">
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
            </div>
            <div class="form-group">
            <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
            <div class="col-lg-8">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            </div>
            <div class="form-group">
            <label for="confirm-password" class="col-lg-4 control-label">Confirm password <span class="require">*</span></label>
            <div class="col-lg-8">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
            </div>
        </fieldset>
        
        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
            <button type="submit" class="btn btn-primary">Register</button>
            </div>
        </div>
        </form>
    </div>
    <div class="col-md-4 col-sm-4 pull-right">
        <div class="form-info">
        <h2><em>RULES </em> AND REGULATIONS</h2>
        <h3>KASABUTAN:</h3>
              <ul>
                <li>Obligasyon nga P105 ang pundo kon gusto moapil.</li>
                <li>Kinahanglan klarohon gayud ang paglista ang member sa maong kapunungan.</li>
                <li>Kinahanglan nga kon ato man sakopon ang atong inahan ug amahan nga biyodo ug biyuda, kinahanglan gyud nga naa sa atoang kamot.</li>
                <li>Death Certificate</li>
                <li>Kinahanglan nga ang i-apil nga anak sa original nga membro, walay labot o kalbi.in.</li>
                <li>60 years old below mag ihap siya ug 30 days ayha siya ma regular member. And 60 pataas mag ihap siya 90 days ayha siya ma regular member. </li>
              </ul>
              <p>Dayon kung mapakyas ka sa pagbayad sa lim k adlaw  gikan sa pagkhitabo, mobalik ka pgka new member. Moihap ka ug 30 days usa ka maregular member ug usab . Sa panahong nga suspended ka sa maong kapunungng waalay tulobagon o bayronon kon adunay nahitabo sa pamilya nga imong gipamembro  </p>

        <a href="/about" class="btn btn-default">More details</a>
        </div>
    </div>
    </div>
</div>
@endsection
