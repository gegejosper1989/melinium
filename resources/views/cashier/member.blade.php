@extends('layout.cashier')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Member</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Member</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        @if(Session::has('success'))
          <div class="col-md-12">
              <div class="alert alert-success">
                  {{ Session::get('success') }}
                  @php
                  Session::forget('success');
                  @endphp
              </div>
          </div>
          @endif
          <div class="col-md-4">
          
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Account Details</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body p-0">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{asset('img/profile.png')}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$data_member->user->name}}</h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Member Type</b> <a class="float-right">{{$data_member->member_type}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Address</b> <a class="float-right">{{$data_member->address}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Birth</b> <a class="float-right">{{$data_member->dob}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Gender</b> <a class="float-right">{{$data_member->gender}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Civil Status</b> <a class="float-right">{{$data_member->civil_status}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Status</b> <a class="float-right">{{$data_member->status}}</a>
                  </li>
                  
                </ul>
              </div>
             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Payment History</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              
                <table class="table table-striped">
                  <thead>
                    <tr> 
                      <th>Deceased</th>
                      <th>Member Type</th>
                      <th>Amount</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_member as $Member)
                    <tr>
                     
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Beneficiaries</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                        Session::forget('success');
                        @endphp
                    </div>
                @endif

                <table class="table table-striped">
                  <thead>
                    <tr> 
                      <th>Name</th>
                      <th>Age</th>
                      <th>Birthday</th>
                      <th>Relationship</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_beneficiary as $Beneficiary)
                    <?php 
                    $dob = strtotime($Beneficiary->dob);       
                    $tdate = time();
                    ?>
                    <tr>
                     <td>{{strtoupper($Beneficiary->lname)}}, {{strtoupper($Beneficiary->fname)}} {{strtoupper($Beneficiary->mname)}}</td>
                     
                     <td>{{date('Y', $tdate) - date('Y', $dob)}}</td>
                     <td>{{$Beneficiary->dob}}</td>
                     <td>{{$Beneficiary->relationship}}</td>
                     <td>
                      <a href="javascript:;" class="edit-beneficiary btn btn-sm btn-info" 
                            data-id="{{$Beneficiary->id}}" 
                            data-fname="{{$Beneficiary->fname}}"
                            data-lname="{{$Beneficiary->lname}}"
                            data-mname="{{$Beneficiary->mname}}"
                            data-dob="{{$Beneficiary->dob}}"
                            data-relationship="{{$Beneficiary->relationship}}"
                            data-gender="{{$Beneficiary->gender}}"
                
                            >
                            <i class="fas fa-pen"> </i>
                        </a>
                     </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <div id="member_modal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Member</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="First Name">First Name</label>
              <input type="text" class="form-control" id="edit_fname" name="edit_fname" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Middle Name">Middle Name</label>
              <input type="text" class="form-control" id="edit_mname" name="edit_mname" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Last Name">Last Name</label>
              <input type="text" class="form-control" id="edit_lname" name="edit_lname" required>
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="Birth Date">Birth Date</label>
              <input type="date" class="form-control" id="edit_dob" name="edit_dob" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Gender">Gender</label>
                <div class="row">
                  <div class="form-check col-md-6  pl-5">
                    <input class="form-check-input" type="radio" name="edit_gender" value="male" required>
                    <label class="form-check-label">Male</label>
                  </div>
                  <div class="form-check col-md-6 text-left">
                    <input class="form-check-input" type="radio" name="edit_gender" value="female" required>
                    <label class="form-check-label">Female</label>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Civil Status">Civil Status</label>
                <select class="form-control" name="edit_civil_status">
                    <option>Single</option>
                    <option>Married</option>
                    <option>Separated</option>
                    <option>Widowed</option>
                </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="Contact Number">Contact Number</label>
              <input type="number" class="form-control" id="edit_contact_number" name="edit_contact_number" placeholder="Contact Number" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Member Type">Member Type</label>
              <select class="form-control" name="edit_member_type">
                  <option>non-regular</option>
                  <option>regular</option>
              </select>
            </div>
          </div>  
          <div class="col-md-4">
            <div class="form-group">
              <label for="Address">Address</label>
              <input type="text" class="form-control" id="edit_address" name="edit_address" placeholder="Address" value="{{ old('address') }}" required>
            </div>
          </div>  
        </div>
      <div class="modal-footer justify-content-between">
        <input type="hidden" class="form-control" id="edit_id" name="edit_id">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary edit" data-dismiss="modal">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</div>
<div id="beneficiary_modal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Benefeciary</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="First Name">First Name</label>
              <input type="text" class="form-control" id="edit_beneficiary_fname" name="edit_beneficiary_fname" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Middle Name">Middle Name</label>
              <input type="text" class="form-control" id="edit_beneficiary_mname" name="edit_beneficiary_mname" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Last Name">Last Name</label>
              <input type="text" class="form-control" id="edit__beneficiary_lname" name="edit__beneficiary_lname" required>
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="Birth Date">Birth Date</label>
              <input type="date" class="form-control" id="edit_beneficiary_dob" name="edit_beneficiary_dob" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Gender">Gender</label>
                <div class="row">
                  <div class="form-check col-md-6  pl-5">
                    <input class="form-check-input" type="radio" name="edit_beneficiary_gender" value="male" required>
                    <label class="form-check-label">Male</label>
                  </div>
                  <div class="form-check col-md-6 text-left">
                    <input class="form-check-input" type="radio" name="edit_beneficiary_gender" value="female" required>
                    <label class="form-check-label">Female</label>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="Relationship">Relationship</label>
                      
              <select name="edit_beneficiary_relationship" id="edit_beneficiary_relationship" class="form-control" required>
                <option></option>
                <option>Wife</option>
                <option>Husband</option>
                <option>Son</option>
                <option>Daugther</option>
                <option>Mother</option>
                <option>Father</option>
                <option>Sibling</option>
              </select>
            </div>
          </div>
        </div>
      <div class="modal-footer justify-content-between">
        <input type="hidden" class="form-control" id="edit_beneficiary_id" name="edit_beneficiary_id">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary update_beneficiary" data-dismiss="modal">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</div>
<div class="modal fade" id="delete_modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete Member</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Member?</p>
        <input type="hidden" class="form-control" id="delete_id" name="delete_id">
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger delete" data-dismiss="modal">Delete</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/member.js') }}"></script>
@endsection