@extends('layout.Cashier')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

          <div class="col-md-12">


            <div class="card ">
              <div class="card-header ">
               
                <div class="text-center">
                
              </div>
              <div class="row">
               
                <div class="col-md-3 col-sm-6 col-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="far fa-credit-card"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Monthly Payments</span>
                      <span class="info-box-number">{{number_format($total_monthly_payment, 2)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-danger"><i class="fas fa-user-times"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Deceased Member</span>
                      <span class="info-box-number">{{$data_deceased_member}}</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-success"><i class="far fa-user"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Total Member</span>
                      <span class="info-box-number">{{$data_total_member}}</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
              </div>
              </div>
              <!-- /.card-header -->
            
            </div>
            <!-- /.card -->
          </div>
          <div class="col-lg-4">
              <div class="card ">
                <div class="card-header ">
                  <h3 class="card-title text-center">Recent Payment</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <tr>
                    <th>
                      Payee
                    </th>
                    <th>Amount</th>
                    
                  </tr>
                  @forelse($data_payment as $Payment)
                  <tr>
                    <td>{{strtoupper($Payment->member->lname)}}, {{strtoupper($Payment->member->fname)}} {{strtoupper($Payment->member->mname)}}</td>
                    <td>{{$Payment->amount}}</td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="2"> <em>No Data</em></td>
                  </tr>
                  @endforelse
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

@endsection