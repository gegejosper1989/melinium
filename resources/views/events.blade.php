@extends('layouts.page')

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Events</h3> <br>
        
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
        
        {{ $data_event->links() }}
        <table class="table table-striped">
            <thead>
            <tr>
                
                <th>Title</th>
                <th>Details</th>
               
            </tr>
            </thead>
            <tbody class="memberresult">
            @forelse($data_event as $Event)
            <tr class="row{{$Event->id}}">
                <td>{{$Event->title}}</td>
                <td>{{$Event->message}}</td>
            </tr>
            @empty
            <tr>
                <td colspan="5" class="text-center"> <em>No Data</em></td>
            </tr>
            @endforelse
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    </div>
</div>
    
@endsection
