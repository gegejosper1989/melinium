@extends('layout.member')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Home</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <!-- <li class="breadcrumb-item active">Dashboard</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header text-center">
                <h3 class="card-title ">RULES AND REGULATIONS</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body p-0">
              <div class="card-body box-profile">
              <p>Kining maong kapunungan gitoyo pagtukod sa mga opisyalis sa purok MILLIENIUM. Sa tumong nga danali-ang tabang ngadto sa miyembro nga mo panaw ngadto; sa laing kalibotan kon tawagon na siya sa kmtayon. Ug kining maong kapunungan, gipahuman ug mg balaod nga angy gayod ipatuman diha ang mga balod nga gikasabutan sa mg opiyalis s purok ug ma membro.. </p>
              <h3>KASABUTAN:</h3>
              <ul>
                <li>Obligasyon nga P105 ang pundo kon gusto moapil.</li>
                <li>Kinahanglan klarohon gayud ang paglista ang member sa maong kapunungan.</li>
                <li>Kinahanglan nga kon ato man sakopon ang atong inahan ug amahan nga biyodo ug biyuda, kinahanglan gyud nga naa sa atoang kamot.</li>
                <li>Death Certificate</li>
                <li>Kinahanglan nga ang i-apil nga anak sa original nga membro, walay labot o kalbi.in.</li>
                <li>60 years old below mag ihap siya ug 30 days ayha siya ma regular member. And 60 pataas mag ihap siya 90 days ayha siya ma regular member. </li>
              </ul>
              <p>Dayon kung mapakyas ka sa pagbayad sa lim k adlaw  gikan sa pagkhitabo, mobalik ka pgka new member. Moihap ka ug 30 days usa ka maregular member ug usab . Sa panahong nga suspended ka sa maong kapunungng waalay tulobagon o bayronon kon adunay nahitabo sa pamilya nga imong gipamembro  </p>
              </div>
             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection