@extends('layout.member')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Member</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Member</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        @if(Session::has('success'))
          <div class="col-md-12">
              <div class="alert alert-success">
                  {{ Session::get('success') }}
                  @php
                  Session::forget('success');
                  @endphp
              </div>
          </div>
          @endif
          <div class="col-md-4">
          
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Account Details</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body p-0">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{asset('img/profile.png')}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$data_member->user->name}}</h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Member Type</b> <a class="float-right">{{$data_member->member_type}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Address</b> <a class="float-right">{{$data_member->address}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Birth</b> <a class="float-right">{{$data_member->dob}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Gender</b> <a class="float-right">{{$data_member->gender}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Civil Status</b> <a class="float-right">{{$data_member->civil_status}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Status</b> <a class="float-right">{{$data_member->status}}</a>
                  </li>
                  
                </ul>
              </div>
             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Payment History</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              
                <table class="table table-striped">
                  <thead>
                    <tr> 
                      <th>Deceased</th>
                      <th>Member Type</th>
                      <th>Amount</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_member as $Member)
                    <tr>
                     
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Beneficiaries</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                        Session::forget('success');
                        @endphp
                    </div>
                @endif

                <table class="table table-striped">
                  <thead>
                    <tr> 
                      <th>Name</th>
                      <th>Age</th>
                      <th>Birthday</th>
                      <th>Relationship</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_beneficiary as $Beneficiary)
                    <?php 
                    $dob = strtotime($Beneficiary->dob);       
                    $tdate = time();
                    ?>
                    <tr>
                     <td>{{strtoupper($Beneficiary->lname)}}, {{strtoupper($Beneficiary->fname)}} {{strtoupper($Beneficiary->mname)}}</td>
                     
                     <td>{{date('Y', $tdate) - date('Y', $dob)}}</td>
                     <td>{{$Beneficiary->dob}}</td>
                     <td>{{$Beneficiary->relationship}}</td>
                     <td>
                      <a href="javascript:;" class="edit-beneficiary btn btn-sm btn-info" 
                            data-id="{{$Beneficiary->id}}" 
                            data-fname="{{$Beneficiary->fname}}"
                            data-lname="{{$Beneficiary->lname}}"
                            data-mname="{{$Beneficiary->mname}}"
                            data-dob="{{$Beneficiary->dob}}"
                            data-relationship="{{$Beneficiary->relationship}}"
                            data-gender="{{$Beneficiary->gender}}"
                
                            >
                            <i class="fas fa-pen"> </i>
                        </a>
                     </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
   

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/member.js') }}"></script>
@endsection