@extends('layout.member')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Members</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Members</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Members List</h3> <br>
                <div class="input-group col-md-4 p-0">
                    <input id="search" type="text" type="search" name="search" class="form-control" placeholder="Search Member">
                    {{ csrf_field() }}
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              
              {{ $data_member->links() }}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Full Name</th>
                      <th>Address</th>
                      <th>Member Type</th>
                      <th>Status</th>
                      <th class="no-print">Action</th>
                    </tr>
                  </thead>
                  <tbody class="memberresult">
                    @forelse($data_member as $Member)
                    <tr class="row{{$Member->id}}">
                      <td><a href="/member/member/{{$Member->id}}">{{strtoupper($Member->lname)}}, {{strtoupper($Member->fname)}} {{strtoupper($Member->mname)}} </a></td>
                      <td>{{$Member->address}}</td>
                      <td>{{$Member->member_type}}</td>
                      <td>{{$Member->status}}</td>
                      <td class="no-print">
                          <a href="/member/member/{{$Member->id}}" class="btn btn-info btn-small" ><i class="btn-icon-only fas fa-search"> </i></a>
                          
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
                
                <div style="margin:20px;">
                  <button class="btn btn-info btn-sm no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>

                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

<!-- /.modal -->
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('member/members_search')}}',
    data:{'search':$value},
    success:function(data){
      $('.memberresult').html(data);
    } 
  });
})
</script> 
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/member.js') }}"></script>
@endsection