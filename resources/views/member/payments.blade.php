@extends('layout.member')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Payments</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Payments</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-12">
           

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Payments List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
        
              {{ $data_payment->links() }}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Date</th>
                     
                      <th>Amount</th>
                      <th>Payment Type</th>
                      <th>Deceased</th>
                      <th>Status</th>
                     
                    </tr>
                  </thead>
                  <tbody class="paymentresult">
                    @forelse($data_payment as $Payment)
                    <tr class="row{{$Payment->id}}">
                      <td>{{$Payment->created_at->format('d-m-Y')}}</td>
                      
                      <td>{{number_format($Payment->amount,2)}}</td>
                      <td>{{$Payment->payment_type}}</td>
                      <td>
                        @if($Payment->deceased_id == 0)
                        N/A
                        @else
                        
                          @if($Payment->member_type == 'member' && $Payment->payment_type == 'deceased')
                            <a href="/member/deceased/{{$Payment->deceased_id}}"> {{strtoupper($Payment->deceased_member->lname)}}, {{strtoupper($Payment->deceased_member->fname)}} {{strtoupper($Payment->deceased_member->mname)}} </a>
                                
                          @elseif($Payment->member_type == 'beneficiary' && $Payment->payment_type == 'deceased')
                            <a href="/member/deceased/{{$Payment->deceased_id}}"> {{strtoupper($Payment->deceased_beneficiary->lname)}}, {{strtoupper($Payment->deceased_beneficiary->fname)}} {{strtoupper($Payment->deceased_beneficiary->mname)}} </a>
                          @else
                          @endif
                        
                        @endif
                      </td>
                      <td>
                        {{$Payment->payment_status}}
                      </td>
                      
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center"> <em>No Data</em></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div class="modal fade" id="payment_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Contribution</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h5>Member</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Last Name">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" readonly>
                    </div>
                </div>
            </div>
            <hr>
            <h5>Deceased</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" class="form-control" id="dfname" name="dfname" placeholder="First Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" class="form-control" id="dmname" name="dmname" placeholder="Middle Name" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Last Name">Last Name</label>
                    <input type="text" class="form-control" id="dlname" name="dlname" placeholder="Last Name" readonly>
                    </div>
                </div>
            </div>
            <hr>
            <h5>Contribution</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Amount">Amount</label>
                    <input type="text" class="form-control" id="amount" name="amount">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="Date">Date</label>
                    <input type="date" class="form-control" id="payment_date" name="payment_date" value="{{date('Y-m-d')}}">
                    </div>
                </div>
                
            </div>
            <input type="hidden" class="form-control" id="payment_id" name="payment_id">
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success processpayment-cashier" data-dismiss="modal">Process Payment</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="success_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Process success!</h4>
            <button type="button" class="close close-success" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Member successfully paid it's contribution.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success text-right close-success" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('cashier/payments_search')}}',
    data:{'search':$value},
    success:function(data){
      $('.paymentresult').html(data);
    } 
  });
})
</script> 
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/payment.js') }}"></script>
@endsection