@extends('layout.member')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Setting</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Setting</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-md-4">
                @if(Session::has('success'))
                  <div class="alert alert-success">
                      {{ Session::get('success') }}
                      @php
                      Session::forget('success');
                      @endphp
                  </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Update Account Details</h3>
                    </div>
                    
                    <!-- /.card-header -->
                    <form action="{{route('save_setting_member')}}" method="post">
                      <div class="card-body">
                          <div class="input-group">
                              <input id="name" type="text" name="name" class="form-control" placeholder="Full Name" value="{{$data_user->name}}" required>
                              {{ csrf_field() }}
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="input-group">
                              <input id="username" type="text" name="username" class="form-control" placeholder="Username" value="{{$data_user->username}}" required>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="input-group">
                              <input id="email" type="email" name="email" class="form-control" placeholder="Email" value="{{$data_user->email}}" required>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="input-group">
                              <input id="password" type="password" name="password" class="form-control" placeholder="Password">
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="input-group">
                              <input id="user_id" type="hidden" name="user_id" value="{{$data_user->id}}" class="form-control" required>
                              <button type="submit" class="btn btn-success btn-sm"> <i class="fas fa-save"></i> Save</button>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection