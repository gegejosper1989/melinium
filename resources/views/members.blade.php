@extends('layouts.page')

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Members List</h3> <br>
        <div class="input-group col-md-4 p-0">
            <input id="search" type="text" type="search" name="search" class="form-control" placeholder="Search Member">
            {{ csrf_field() }}
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
        
        {{ $data_member->links() }}
        <table class="table table-striped">
            <thead>
            <tr>
                
                <th>Full Name</th>
                <th>Address</th>
                <th>Member Type</th>
                <th>Status</th>
                
            </tr>
            </thead>
            <tbody class="memberresult">
            @forelse($data_member as $Member)
            <tr class="row{{$Member->id}}">
                <td>{{strtoupper($Member->lname)}}, {{strtoupper($Member->fname)}} {{strtoupper($Member->mname)}}</td>
                <td>{{$Member->address}}</td>
                <td>{{$Member->member_type}}</td>
                <td>{{$Member->status}}</td>
                
            </tr>
            @empty
            <tr>
                <td colspan="5" class="text-center"> <em>No Data</em></td>
            </tr>
            @endforelse
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    </div>
</div>
    
@endsection
