<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/thank-you', 'HomeController@thank_you');

Auth::routes();
Route::get('/admin', 'HomeController@log_in');
Route::get('/about', 'HomeController@about');
Route::get('/members', 'HomeController@members');
Route::get('/events', 'HomeController@events');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' =>'admin_auth','prefix' => 'admin'], function(){
    Route::get('/dashboard', 'AdminController@index');
    Route::get('/members', 'AdminController@members');
    Route::get('/claims', 'AdminController@claims');
    Route::get('/members_search', 'AdminController@members_search');
    Route::get('/deceased', 'AdminController@deceased');
    Route::get('/deceased/{deceased_id}', 'DeceasedController@view_deceased');
    Route::post('/deceased/process', 'DeceasedController@deceased_process');
    Route::get('/member_search_deceased', 'DeceasedController@member_search');
    Route::get('/beneficiary_search_deceased', 'DeceasedController@beneficiary_search_deceased');
    Route::get('/members/filter/{filter_type}', 'MemberController@filter_member_active');
    Route::post('/member/add', 'AccountController@add_member')->name('add_member');
    Route::post('/member/edit', 'AccountController@edit_member')->name('edit_member');
    Route::get('/member/{member_id}', 'AccountController@view_member')->name('view_member');
    Route::get('/member/deactivate/{member_id}', 'AccountController@deactivate')->name('deactivate');
    Route::get('/member/activate/{member_id}', 'AccountController@activate')->name('activate');
    Route::get('/member/regular/{member_id}', 'MemberController@regular')->name('regular');
    Route::get('/member/approve/{member_id}', 'MemberController@approve')->name('approve');
    Route::get('/member/cancel/{member_id}', 'MemberController@cancel')->name('cancel');
    Route::get('/registrations/filter/{member_type}', 'MemberController@filter_registration_member')->name('filter_registration_member');
    Route::post('/member/delete', 'AccountController@delete_member')->name('delete_member');
    Route::post('/benefeciaries/add', 'BenefeciaryController@add_benefeciaries')->name('add_benefeciaries');
    Route::post('/beneficiary/edit', 'BenefeciaryController@edit_benefeciaries')->name('edit_benefeciaries');
    

    Route::get('/payments', 'AdminController@payments');
    Route::post('/payment/process', 'PaymentController@process_payment');
    Route::get('/payment/filter/{payment_type}', 'PaymentController@filter_payments');
    Route::get('/payments_search', 'AdminController@payments_search');

    Route::post('/contribution/process', 'ContributionController@process_contribution');
    Route::get('/notifications', 'AdminController@notifications');
    Route::get('/reports', 'AdminController@reports');
    Route::get('/report/deceased', 'ReportController@report_deceased');
    Route::post('/report/deceased/range', 'ReportController@report_range_deceased')->name('report_range_deceased');
    Route::get('/report/members', 'ReportController@report_member');
    Route::post('/report/members/range', 'ReportController@report_range_members')->name('report_range_members');
    Route::get('/report/contribution', 'ReportController@report_contribution');
    Route::post('/report/contribution/range', 'ReportController@report_range_contribution')->name('report_range_contribution');
    Route::get('/settings', 'AdminController@settings');
    Route::post('/settings/update', 'AdminController@settings_update')->name('settings_update');
    Route::get('/broadcasts', 'AdminController@broadcasts');
    Route::post('/broadcast/add', 'BroadcastController@add_broadcast')->name('add_broadcast');
    Route::get('/staffs', 'AdminController@staffs');
    Route::post('/staffs/add', 'StaffController@add_staff')->name('add_staff');
    Route::post('/staff/edit', 'StaffController@edit_staff')->name('edit_staff');
    Route::get('/events', 'AdminController@events');
    Route::post('/events/add', 'EventController@add_events')->name('add_events');
    Route::post('/event/edit', 'EventController@update_event')->name('update_event');
    Route::post('/event/delete', 'EventController@delete_event')->name('update_event');
    Route::get('/registrations', 'AdminController@registrations');

});

Route::group(['middleware' =>'cashier_auth','prefix' => 'cashier'], function(){
    Route::get('/members_search', 'CashierController@members_search');
    Route::get('/member_search_deceased', 'DeceasedController@member_search_cashier');
    Route::get('/beneficiary_search_deceased', 'DeceasedController@beneficiary_search_deceased__cashier');
    Route::post('/deceased/process', 'DeceasedController@deceased_process_cashier');
    Route::get('/deceased/{deceased_id}', 'DeceasedController@view_deceased_cashier');
    Route::get('/home', 'CashierController@index');
    Route::get('/members', 'CashierController@members');
    Route::get('/member/{member_id}', 'CashierController@view_member')->name('view_member');
    Route::get('/payments', 'CashierController@payments');
    Route::get('/notifications', 'CashierController@notifications');
    Route::get('/reports', 'CashierController@reports');
    Route::get('/setting', 'CashierController@setting');
    Route::post('/contribution/process', 'ContributionController@process_contribution_cashier');
    Route::get('/payments_search', 'CashierController@payments_search');
    Route::post('/payment/process', 'PaymentController@process_payment_cashier');
    Route::post('/save_setting', 'CashierController@save_setting')->name('save_setting');
});

Route::group(['middleware' =>'member_auth','prefix' => 'member'], function(){
    Route::get('/home', 'MemberController@index');
    Route::get('/members', 'MemberController@members');
    Route::get('/member/{member_id}', 'MemberController@view_member')->name('view_member');
    Route::get('/members_search', 'MemberController@members_search');
    Route::get('/payments', 'MemberController@payments');
    Route::get('/notifications', 'MemberController@notifications');
    Route::get('/setting', 'MemberController@setting');
    Route::post('/save_setting_member', 'MemberController@save_setting_member')->name('save_setting_member');
});

Route::post('/register_process', 'RegisterController@register_account')->name('register_account');